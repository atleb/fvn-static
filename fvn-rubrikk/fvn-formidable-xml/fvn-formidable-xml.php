<?php
/*
Plugin Name: FVN Formidable XML
Plugin URI: http://blogg.fvn.no/beta/formidable
Description: Plugin to replicate AIDA xml generation with Formidable Forms as frontend. Assumes cron job for ftp
Version: 0.9
Author: Atle Brandt
Author URI: http://twitter.com/atleb
License: GPL2
*/
global $FVN_fxml;
global $wp_roles;
require_once(ABSPATH . 'wp-admin/includes/file.php');
// define('WP_DEBUG',true); //skip for less verbose err
define('FVN_FORMIDABLE_XML_TITLE','FVN Formidable XML');
define('FVN_FORMIDABLE_XML_NAME','fvn-formidable-xml');
define('FVNFX_PATH',WP_PLUGIN_DIR.'/'.FVN_FORMIDABLE_XML_NAME);
define('FVNFX_CLASS_PATH',FVNFX_PATH.'/classes');
define('FVNFX_URL', WP_PLUGIN_URL . '/' . FVN_FORMIDABLE_XML_NAME);
define('FVNFX_SCRIPT_PATH',FVNFX_URL .'/javascript');

include_once(FVNFX_CLASS_PATH . "/Fvn_Formidable_Xml.php");
$FVN_fxml = new FVN_fxml(__FILE__);

?>
