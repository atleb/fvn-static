<?php

class FVN_fxml {
	const AUTHOR_MANAGE_CAPABILITY = 'manage_fxml';
	const AUTHOR_ROLE_NAME = 'author';

	function __construct($file)
	{
        //WP default functions
        register_activation_hook($file, array(&$this, 'activate_plugin'));
        register_deactivation_hook($file,  array($this, 'deactivate_plugin'));

        // generic admin actions
        add_action('admin_menu', array($this, 'create_admin_menu'));
        add_action('admin_init', array($this, 'admin_init'));

        // plugin specific actions
        add_action('frm_after_create_entry', array($this, 'generate_FVNFX_xml'), 30, 2);

	}

	/* Run every time we activate the plugin from the Plugins-admin page.
	 * Get the role AUTHOR_ROLE_NAME and add the capability AUTOR_MANAGE_CAPABILITY.
	 * Check the database version and check if we have to install or upgrade.
	 */
	public function activate_plugin()
	{
        error_log('activate FVNFX');
		global $wp_roles;
		// Load wp_roles if we don't have them.
		if ( !$wp_roles ) $wp_roles = new WP_Roles();
		// Make sure we save changes to the database.
		$wp_roles->use_db = true;
		// Add the new capabilities to our role(s)
		$wp_roles->add_cap(self::AUTHOR_ROLE_NAME, self::AUTHOR_MANAGE_CAPABILITY);

        $options = array(
            'papername' => 'FV',
            'paperid' => '60012345'
        );
        
        update_option('FVNFX_options', $options);

	}

	public function deactivate_plugin()
	{
		global $wp_roles;
		if ( !$wp_roles ) $wp_roles = new WP_Roles();
		$wp_roles->use_db = true;
		$wp_roles->remove_cap(self::AUTHOR_ROLE_NAME, self::AUTHOR_MANAGE_CAPABILITY);

		//delete_option(self::DB_VERSION_VARIABLE_NAME, self::DB_VERSION);
		//error_log("deactivate", false);
	}
    public function admin_init()
    {
        register_setting('FVNFX_options_group', 'FVNFX_options');
    }
	public function create_admin_menu()
	{
        add_options_page('FVNFX Settings', 'FVNFX', 'administrator', 'FVNFX_settings', array($this, 'settings_page'));

    }

    public function settings_page()
    {
        $FVNFX_options = get_option('FVNFX_options');
        ?>
        <div class="wrap">
            <h2>Innstillinger for XML generator til AIDA rubrikk</h2>
            <form action="options.php" method="post">
                <?php settings_fields('FVNFX_options_group'); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">papername:</th>
                        <td><input type="text" name="FVNFX_options[papername]" value="<?php echo $FVNFX_options['papername']; ?>" /> <em>(default: FV)</em></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">papyer id:</th>
                        <td><input type="text" name="FVNFX_options[paperid]" value="<?php echo $FVNFX_options['paperid']; ?>" /><em>(test: 60012345)</em></td>
                    </tr>
                </table>
                <p class="submit">
                    <input type="submit" class="button-primary" value="Lagre" />
                </p>
            </form>
        </div>
        <?php
    }


    /*
     * Parsing Formidable form values and saving data as xml, then upload to AIDA via ftp
     */ 
    function generate_FVNFX_xml($entry_id, $form_id){
        global $wpdb, $frmdb;

       /*
       * Formidable form values - id based on creation time - could be saved as array/json in config
       */ 

        $id_formNum = 3;    // Formidable id for FVN
        $id_cat = 22;       //      [10]Hva skal du annonsere
        $id_bImg = 24;      //      [12]Ønsker du bilde i annonsen?
        $id_title = 25;     //      [13]Tittle
        $id_text1 = 26;     //      [14]Tekst / m bilde
        $id_text2 = 27;     //      [15]Tekst u / bilde
        $id_imgU = 57;      //      [93]Last opp bilde
        $id_date = 58;      //      [11]Innrykksdato

        if($form_id == $id_formNum){
            //local var for simpler assignments and validation 
            $f_elems = $_POST['item_meta'];
 
            if( isset($f_elems[$id_cat]) && isset($f_elems[$id_date]) && isset($f_elems[$id_title]) ) {
            
                $adCat = $f_elems[$id_cat];
                // category,subcategory as value for better UX frontend - xml needs two fields [0],[1]
                $adCats = explode(",", $adCat); 
                //reformatting readable jQ UI date into AIDA requiered YYYYMMDD format
                $adDate = $f_elems[$id_date]; 
                $dateParts = explode(".", $adDate);
                $adPrintDate = $dateParts[2].$dateParts[1].$dateParts[0];
                //truncate Tittel – 24 chars max length (validated on frontend as well)
                $adTitle = substr($f_elems[$id_title],0,24);
            }

            if( isset($f_elems[$id_bImg]) ){
                // value labels == "Med" || "Uten" determines which text field and allowed length (validated front end as well)
                $adHasImg = substr($f_elems[$id_bImg],0,3);
                $adText = "";
                $adImageName = "";

            // dummy img name for test ftp
            $adImageName = '';

                if ( $adHasImg == "Med"){
                    if(isset($f_elems[$id_text1])){ $adText = substr($f_elems[$id_text1],0,132); }
                    // get image name from upload field, loop since Formidable supports multi-upload
                    $media_ids = $f_elems[$id_imgU];
                    foreach ( (array)$media_ids as $fileid ) {
                        if ( ! $fileid ) { continue; }
                        $adImageName = basename( get_attached_file( $fileid ) );
                    }

                } else {
                    if(isset($f_elems[$id_text2])){ $adText = substr($f_elems[$id_text2],0,225); }
                }
            }
              
            // paper name + payer id from options object FVNFX_options
            $options = get_option('FVNFX_options');
            $paperName = $options['papername'];
            $paperId = $options['paperid'];
            $adID = 150000000 + $entry_id ;  // needs to increment and be unique in AIDA terms
            

            $baseXML = '<?xml version="1.0" encoding="UTF-8"?>
                <!DOCTYPE FINN.IF.PAPERAD SYSTEM "http://www.finn.no/dtd/FINNIF-paperad33.dtd">
                <FINN.IF.PAPERAD>
                <HEAD>
                    <NEWSPAPER_ID>' . $paperName . '</NEWSPAPER_ID>
                    <SENDER_ID>FINN</SENDER_ID>
                </HEAD>
                <OBJECT>
                    <PAYER_ACCOUNTNO>'. $paperId .'</PAYER_ACCOUNTNO>
                    <PAYER_NAME>' . $paperName . ' Integration</PAYER_NAME>
                    <ADVERTISER_ACCOUNTNO>'. $paperId .'</ADVERTISER_ACCOUNTNO>
                    <ADVERTISER_NAME>' . $paperName . ' Integration</ADVERTISER_NAME>
                     
                    <FINN_NEWSPAPER_REF>' . $adID . '</FINN_NEWSPAPER_REF>
                    <FINN_INTERNAL_ID>' . $adID . '</FINN_INTERNAL_ID>
                    <FINN_SEARCHCODE>' . $adID . '</FINN_SEARCHCODE>
                    <PRINT_DATE>' . $adPrintDate . '</PRINT_DATE>

                    <PRODUCT_TYPE>Plusspakke</PRODUCT_TYPE>
                    <AD_TYPE VALUE="graphic"></AD_TYPE>
                    <AD_CATEGORY>' . $adCats[0] . '</AD_CATEGORY>
                    <AD_SUBCATEGORY1>' . $adCats[1] . '</AD_SUBCATEGORY1>

                    <AD_HEADING><![CDATA[' . $adTitle . ']]></AD_HEADING>
                    <AD_TEXT><![CDATA[' . $adText . ']]></AD_TEXT>
                    <ORDER_DATE/>
                    <MO REF="' . $adImageName . '" PRIORITY="1" MMO_TYPE="image"></MO>
                </OBJECT>
                </FINN.IF.PAPERAD>';

            /* 
            * Save baseXML to filesystem inside Formidable folder for current site, also prepare path variable for image upload
            * $upload_dir['path'] => .../wp-content/uploads/2015/05/  
            * $upload_dir['basedir'] => .../wp-content/uploads/sites/9  
            */
            $adBaseFolder = 'formidable/'; 

            $access_type = get_filesystem_method();
            if($access_type === 'direct') {
                $creds = request_filesystem_credentials(site_url() . '/wp-admin/', '', false, false, array()); //swap to wp-content..
                if ( ! WP_Filesystem($creds) ) {  return false; } //error logging, state on entry?

                global $wp_filesystem;
                $upload_dir = wp_upload_dir();
                $adPath = trailingslashit($upload_dir['basedir']) . $adBaseFolder;
                
                $xmlName = $paperName . $adID . '.xml'; 
                $filename =  $adPath . $xmlName; 
                $adImagePathName = $adPath . $adImageName; 
                
                $wroteXML = $wp_filesystem->put_contents( $filename, $baseXML, FS_CHMOD_FILE);
                if ( ! $wroteXML ) {  return false;   } //error logging, state on entry?
            }

        } //end form conditional handling
    } 

}

?>