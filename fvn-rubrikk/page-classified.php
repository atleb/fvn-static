<?php
/*
Template Name: Classified testing
*/
?>
<?php 
/* 
* simplified template for running php snippets inside WP context
*/
?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><meta charset='utf-8'/></head>

<body>
<?php
$access_type = get_filesystem_method();
if($access_type === 'direct') {
	/* you can safely run request_filesystem_credentials() without any issues and don't need to worry about passing in a URL */
	$creds = request_filesystem_credentials(site_url() . '/wp-admin/', '', false, false, array());

	/* initialize the API */
	if ( ! WP_Filesystem($creds) ) {
		/* any problems and we exit */
		echo 'cred problems';
		return false;
	}	

	global $wp_filesystem;
	/* do our file manipulations below */
	// echo 'doing file stuff otto way';

	$upload_dir = wp_upload_dir();

/*
	echo $upload_dir['path'] . '<br />';
	/home/fvn/blogg/htdocs/beta/wp-content/uploads/2015/05
	echo $upload_dir['url'] . '<br />';
	http://blogg.fvn.no/beta/wp-content/uploads/2015/05
	echo $upload_dir['subdir'] . '<br />';
	/2015/05
	echo $upload_dir['basedir'] . '<br />';
	/home/fvn/blogg/htdocs/beta/wp-content/uploads
	echo $upload_dir['baseurl'] . '<br />';
	http://blogg.fvn.no/beta/wp-content/uploads
*/	
	/* 
	* use default current upload directory? 
	* or swap into formidable, then use year, month, mkdir etc: 

	if(!$wp_filesystem->is_dir($plugin_path . '/config/') 
	{
		// directory didn't exist, so let's create it 
		$wp_filesystem->mkdir($plugin_path . '/config/');
	}
	*/

	$filename = trailingslashit($upload_dir['path']).'test.txt';
	echo $filename . '<br />'; 
 	/*		/home/fvn/blogg/htdocs/beta/wp-content/uploads/2015/05/test.txt  	*/
/*
	$testWrote = $wp_filesystem->put_contents( $filename, 'Test file contents', FS_CHMOD_FILE);
	if ( ! $testWrote ) {
	    echo 'error saving file!';
	}
*/
}	

/*
* FTP-server: FinnAnnTest
* TODO: 
* 	easy swap live vs test config 
*	save pwd outside bitbucket
*/
/*
* needs SCHIT feedback
*
// set up a connection or die
$ftp_server = "ftp.aftenposten.no";
$ftp_user_name = "FinnAnnTest";
$ftp_user_pass = "li8mju7cft6";

$conn_id = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server"); 
// login with username and password
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

//test config static file
$xmlName = '20150505_WP01.xml';
$file = trailingslashit($upload_dir['path']). $xmlName;
$remote_file = $xmlName;

// upload a file
if (ftp_put($conn_id, $remote_file, $file, FTP_ASCII)) {
 echo "successfully uploaded $file\n";
} else {
 echo "There was a problem while uploading $file\n";
}

// close the connection
ftp_close($conn_id);
*/



?>
	<div id="primary" class="content-area">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>
			</div><!-- .entry-content -->

		<?php endwhile; // end of the loop. ?>

	</div><!-- #primary -->

</body>
</html>