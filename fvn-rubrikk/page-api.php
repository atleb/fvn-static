<?php
/*
Template Name: API converter
*/
?>
<?php /* Start loop 

// function.php update for query vars
// id
// displayPublication - then can use to make one fetcher, route to sysla, krsby etc
function add_query_vars_filter( $vars ){
  $vars[] = "id";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

// then can use to get props
*/
$fasten_id_full = get_query_var( 'id', 1 ); 
$fasten_id = substr($fasten_id_full, 3);

//and replace with cusotm loop;
// WP_Query arguments
// 'p' for post id over page_id
$args = array (
	'page_id'                => $fasten_id,
	'post_status'            => 'publish',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
		// do something
?>
<html xmlns='http://www.w3.org/1999/xhtml'><head><meta charset='utf-8'/>
<title><?php the_title(); ?></title>
<?php /* todo: determine canonical and share url setup */ ?>
<link rel='canonical' href='http://blogg.fvn.no/beta/add-some-title<?php echo $fasten_id ?>.html'/>
<link rel='story-share-url' href='http://www.fvn.no/article/<?php echo $fasten_id ?>.html'/>
</head>

<body>
<article itemscope='' itemtype='http://schema.org/Article' 
<?php /* todo: get from cutom props or config */ ?>
	data-type='standard' 
	data-paywall-state='open' 
	data-comment-system='facebook' 
	data-version='1'>

		<section class='article-header'>
  			<h1 itemprop='name headline' class='article-title'><?php the_title(); ?></h1>
  		</section>
  		<section class='byline'>
  			<?php /* todo: get from tags? or cats?  */ ?>
  			<span class='section' 
  				data-section-id='lokalt/' 
  				data-section-numerical-id='40'>Nyheter</span>
  		</section>
  		<div itemprop='articleBody' class='article-body'>
			<?php the_content(); ?>
		</div>
		<footer class='article-footer'>
			<?php /* todo: needed or just in byline? */ ?>
			<span class='date published'>Publisert:
			<time class='date published' itemprop='datePublished' datetime='<?php get_the_time("c") ?>'><?php date('c') ?>08:55</time>
			</span>
		</footer>	
</article>
</body>
</html></body>
<?php
	} // end while 
} else {
	// no posts found, return 404 bug
}
// Restore original Post Data
wp_reset_postdata();


?>
