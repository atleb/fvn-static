<?php
//added for testing limited API access
//either allow all (*) or one domain only - ie http://www.fvn.no

function fvn_add_origin($headers) {
	$headers['Access-Control-Allow-Origin'] = "*";
	return $headers;	
}
add_filter('wp_headers','fvn_add_origin');


// function.php update for query vars
// id
// displayPublication - then can use to make one fetcher, route to sysla, krsby etc
function add_query_vars_filter( $vars ){
  $vars[] = "id";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

?>