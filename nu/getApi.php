<?php
// adjustment to NewsClient.php - dedicated funtion to get params
    /**
     * @param $publication
     * @param $section_id
     * @param $accept
     * @return array
     */
    public function get_section_param($publication, $section_id){
        
        $headers = array('Accept' => 'application/json');
        $article_url = "config/publication/" . $publication . "/sections/" . $section_id . "/parameters";
        
        $parameters = array();
            
        $newsClient=$this->execute($article_url, 'GET', $parameters,
            $headers, $response_caching,
            array('cache_time' => NewsClient::DEFAULT_SECTION_DEFINITION_CACHE_TIME));
       
        $response = $newsClient->get_response();        
        $newsClient->json_array = json_decode($response, true);
        
        return $newsClient->json_array;
    }


// useage give a NC setup with publication defined
    $secId = 80;

    $secParams = $newsclient->get_section_param($publication, $secId);
?>    
    test any param[] <? echo $secParams['uniqueName']  ?>

    print all; 
    <pre><? // print_r($secParams)  ?></pre>
