
var taxU = '//lisacache.fvn.no',
 taxP = '/skatt/', taxQ = '', taxT = 'l';

var loadPageVar = function loadPageVar(sVar) {    return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1")); 
};

var countyId = countyId || loadPageVar("i");
var taxType = taxType || loadPageVar("t");
if (taxType !== "" && taxType.length == 1) { taxT = taxType; }
if (countyId === "") { countyId = 901; } 
if (countyId > -1 && countyId < 900) {  taxT = 'l'; taxQ = '?id=' + countyId + '&cb=tax&y=2013';
} else if (countyId > 900 && countyId < 9999 ) { taxQ = '?id=' + countyId + '&t=' + taxT + '&cb=tax&y=2013';} 


var taxDataHeaders = [], taxDataArray = [], $taxData = {}, $taxDataRows = {};


jQuery.ajax({ url: taxU + taxP + taxQ, dataType: "text"}).done( taxList );

function taxList(data){ 

	$taxData = $(data);
	$taxDataRows = $taxData.find("tr");

	$taxDataRows.eq(0).find('th').each(function() { taxDataHeaders.push($(this).text()); });
	$taxDataRows.each(function() {
	    var arrayOfThisRow = {};
	    var tableData = $(this).find('td');
	    if (tableData.length > 0) {
	        tableData.each(function(index) { arrayOfThisRow[taxDataHeaders[index]]=$(this).text(); });
	        taxDataArray.push(arrayOfThisRow);
	    }
	});

	var strTax = "";
	if( taxT == "l") { 
		for (var i = 0; i < taxDataArray.length ; i++) {
			strTax += taxDetailsSpecial(i);
		};
		$(".taxTab").html(strTax);
	}
	else {
		for (var i = 0; i < loadMoreLimit ; i++) {
			strTax += taxDetailsBasic(i);
		};

		$(".taxTab").append(strTax);
		$("#taxWrap").append($('<button id="showRest">vis flere</button>'));
		$("#showRest").on('click', showRestData);
	}
}


var loadMoreLimit = 15;

function taxDetailsSpecial (index) {
	var i = index;
	var strTax = "<li>";
		strTax += taxDataArray[i].Navn + " <em>" + taxDataArray[i].Merknad + "</em><br>";
		strTax += "<span>Inntekt: </span>"+ taxDataArray[i].Inntekt + " ";
		strTax += "<span>Formue: </span>"+ taxDataArray[i].Formue + " ";
		strTax += "<span>Skatt: </span>"+ taxDataArray[i].Skatt + " ";
	strTax += "</li>";

	return strTax;
}

function taxDetailsBasic (index) {
	var i = index;
	var strTax = "<li>";
		strTax += taxDataArray[i].Navn + " (" + taxDataArray[i].FÅr + ")<br>";
		strTax += "<span>Inntekt: </span>"+ taxDataArray[i].Inntekt + " ";
		strTax += "<span>Formue: </span>"+ taxDataArray[i].Formue + " ";
		strTax += "<span>Skatt: </span>"+ taxDataArray[i].Skatt + " ";
	strTax += "</li>";

	return strTax;
}

function showRestData () {
	var strTax = "";
	for (var i = loadMoreLimit; i < taxDataArray.length ; i++) {
		strTax += taxDetailsBasic(i);
	};
	$(".taxTab").append(strTax);
	$("#showRest").hide();
}