<?php
header("Content-Type: application/json; charset=utf-8");
header("access-control-allow-origin: http://www.fvn.no");

$callback = $_GET['cb'];

	if ($callback == 'list'){
		returnList();
	} else if ($callback == 'tax'){
		returnNumbers();
	}

function returnNumbers(){

	$id = $_GET['id'];
	if (is_numeric($id)) {

		if (isset($_GET['t'])) {
			$type = $_GET['t'];
			switch ($type) {
				case 'f':
					$folder = "formue";
					break;
				case 's':
					$folder = "skatt";
					break;
				case 'i':
					$folder = "inntekt";
					break;									
				default:
					$folder = "lister";
					break;
			}
		} else {
			$folder = "lister";
		}

		if (isset($_GET['y']) && is_numeric($_GET['y'])) {
			$year = $_GET['y'];
			$baseFolder = $year."tall";
		} else {
			$baseFolder = "2014tall"; 
		}

		$taxListName = $baseFolder."/".$folder."/".$id.".txt";

		if (file_exists($taxListName)) {
			$fp = fopen($taxListName, "r");
			$content = fread($fp, filesize($taxListName));
			fclose($fp);
		
			echo $content;
		} //else file not ready error msg?
	}
	// no valid id - return bug msg?
}

?>