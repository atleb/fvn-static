<?php
$baseUrl = 'http://lisacache.fvn.no/';
$image_url = $baseUrl . 'sysla/images/';

$getUrl = 'http://www.sysla.no/';
$getPara = '?webwidget=1&type=fvn';

$content = file_get_contents( $getUrl . $getPara);
//echo "<hr>Content ".$content;
$articles = json_decode($content);
//echo "<hr> Art".$articles;

// expect at least 3 articles
if (is_array($articles) && count($articles) >= 3) {
    // we want to delete all old image files, so keep track of the ones we want to keep
    $files_to_keep = array();

    // go through all articles and download image
    foreach ($articles as $article) {
        if (!empty($article->image)) {
            $file = pathinfo($article->image);
            $image_name = md5($file['filename']) . '.' . $file['extension'];
            $files_to_keep[] = $image_name;
            $new_image = $image_url . $image_name;
            $temp_image = file_get_contents($article->image);
            file_put_contents('images/' . $image_name, $temp_image);
            $article->image = $new_image;
        }
    }

    // delete all old files
    foreach (glob("images/*") as $file) {
        if (!in_array(basename($file), $files_to_keep)) {
            unlink($file);
        }
    }
    // save as new json-file
    $articles =  json_encode($articles);
    file_put_contents('sysla_mob.json', $articles);
    $articlesWrap = 'c(' . $articles . ')';
    file_put_contents('sysla_forsiden.json', $articlesWrap);
    
    echo "Bilder lasta ned og ny JSON generert";
} else {
    echo "Noe gikk til helvete! Jim kan hjelpe... ";
}

?>