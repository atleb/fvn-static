<? error_reporting(E_ALL);
    ini_set('display_errors', 1);
    ?>
<?php 
  /**
  * Fædrelandsvennen Åpenhetsportal ("Sørlandsporten")
  * version: 4
  * page: info display via SKD
  * info: first version with simple structure
  */
  $pgTitle = "Om Sørlandsporten";

  include('../head.php');
  include('../menubrand.php');
?>
<?
    require('../../tools/snd-api/newsclient/NewsClient.php');
    if(isset($_REQUEST['id'])){
        $article_id = $_REQUEST['id'];
    } else {
        $article_id = "2612669";
    }

    $publication = "fvn";

    $newsclient = new NewsClient(array(
            'client_id' => 'HSjf56fQS1mNB1nw9gsmewe9F',
            'api_version' => 'v2',
            'cache_time' => 30,
            'cache_enable' => true,
            'cache_directory' => 'cache_dir'
    ));

    $article = $newsclient->get_article($publication,$article_id);
?>
 
<div class="container fullImg" >

    <div class="intro row">
        <li class=''><a href="?id=2606584">Få hjelp til innsyn</a></li>
        <li class=''><a href="?id=2612669">Derfor gjør vi det</a></li>
        <li class=''><a href="?id=2651833">Slik gjorde vi det</a></li>     
        <li class=''><a href="?id=2606586">Dine rettigheter</a></li>
    </div>

    <div class="art-<? echo $article->get_article_type();?>">
        <h1 class="articleTitle"><? echo $article->get_title();?></h1>
        <p class="leadText"><? echo $article->get_leadtext();?></p>

        <?  // Display TOPMEDIAREL: echo is_object($article->get_topmediarel())?$article->get_topmediarel()->get_article_type():"";?>
        <? if(is_object($article->get_topmediarel()) &&  $article->get_topmediarel()->get_article_type() == 'picture'){ ?>
            <? if($article->get_topmediarel() != NULL){?>
                <div><image src="<? echo $article->get_topmediarel()->get_url(ImageWidths::w1024,ImageCrops::c169);?>"></image></div>
            <?}?>
        <? } else if(is_object($article->get_topmediarel()) &&  $article->get_topmediarel()->get_article_type() == 'multimedia') { ?>
            <?  // Fetching the html from an multimedia article
                $relation = $article->get_topmediarel();
                $rel_article = $newsclient->get_article($relation->get_publication(),$relation->get_content_id());
                echo $rel_article->get_html();
            ?>
        <? } else if(is_object($article->get_topmediarel()) &&  $article->get_topmediarel()->get_article_type() == 'video') { ?>
            <?  // Fetching the html from a video
                $relation = $article->get_topmediarel();
                $rel_article = $newsclient->get_article($relation->get_publication(),$relation->get_content_id());
            ?>
            <? echo $rel_article->get_video() ?>
        <? } ?>
        <?  // Its here needed to check for the existence of the get_topmediarel object          
         if(is_object($article->get_topmediarel())){?>
            <p><?echo $article->get_topmediarel()->get_image_caption();?> <?echo $article->get_topmediarel()->get_photographer()!=''?'FOTO: '.$article->get_topmediarel()->get_photographer():'';?></p>
        <?}?>
            
        <?// echo $article->get_body();?>
        <? echo process_inline_elements($article->get_body(false),$article,$newsclient);?>

        <!--  byline   -->
        <p> Av:
            <? foreach($article->get_authors() as $author){ echo $author->get_name() . " "; } ?>
            <strong>Oppdatert: </strong><? echo $article->get_updated();?> 
            <strong>Publisert: </strong> <? echo $article->get_published();?>
        </p>
            
    </div>
</div>

<?php 
  include('../stats.php');
  include('../footer.php');
?>

<?
    function process_inline_elements($src_html,$article,$newsclient){
        $html = str_get_html($src_html);
        if(!empty($src_html)){
            foreach($html->find('img') as $element){
                if(!empty($element->id)){
                    $id_rel = $article->get_relation_by_synth_id(substr($element->id,1));
                    if(($width = $id_rel->get_image_meta(Relation::PICTURESIZE)) != ""){
                        $picture_width = "w".$width;
                    } else {
                        $picture_width = ImageWidths::w280;
                    }
                    if(($size = $id_rel->get_image_meta(Relation::PICTUREFORMAT)) != ""){
                        $picture_crop = "c".$size;
                    } else {
                        $picture_crop = ImageCrops::c169;
                    }
                    // Override default image width on inline images
                    // $element->width = "480";
                    $element->src = $id_rel->get_url($picture_width,$picture_crop);
                    if($id_rel->get_image_meta(Relation::IMAGE_CAPTION) != ""){
                        $element->outertext = $element."<br/><strong>CAPTION: ".$id_rel->get_image_meta(Relation::IMAGE_CAPTION)."</strong><br/>";
                    }
                }
            }
            foreach($html->find('a') as $element){
                $id = substr($element->id,1);
                if(!empty($id)){
                    $id_rel = $article->get_relation_by_synth_id($id);
                    $element->href = "?id=".fetch_after_last_slash($element->href)."&pub=".$article->pub. " ";
                    if(is_object($id_rel && $id_rel->get_article_type() != 'news')){
                        $element->outertext = "<strong>--Relation articleType: ".$id_rel->get_article_type()."---</strong><br/>".$element."<br/>";
                    } else {
                        $element->outertext = $element."<br/><strong>Relation url: </strong>".$id_rel->get_publication()."-".$id_rel->get_content_id() ."<br/>";
                        // Code for fetching inline related article
                        // $rel_article = $newsclient->get_article($id_rel->get_publication(),$id_rel->get_content_id());
                        //print_r($rel_article);
                    }
                }
            }
        }
        return $html;
    }
?>