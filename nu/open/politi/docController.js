(function () {
    'use strict';

    var controllerId = 'docController';
    angular.module('app').controller(controllerId, ['docService', docController]);

    function docController(docService) {
      var dm = this;
      dm.title = 'documents';
      dm.projectid = '21708-politidistriktene';  //'21613-dev';
      dm.activate = activate;

      dm.queries = [];
      dm.curQ = { querystring: '', timestamp: '', projectid: '', resultpage: 0 };
      dm.query = '';

      dm.totalDocs = 0;     
      dm.curDoc = {};
      dm.curPage = 1;
      dm.documents = [];

      dm.getDocuments = getDocuments;
      dm.getMoreDocuments = getMoreDocuments;
      dm.getCurrentDocument = getCurrentDocument;
      dm.isCurDoc = isCurDoc;
      dm.setCurDoc = setCurDoc;
      dm.getCurrentPage = getCurrentPage;
      dm.isCurPage = isCurPage;
      dm.setCurPage = setCurPage;

      dm.unis = [ 
        { pdi:'', name: 'Hele Norge', projectid: '21708-politidistriktene'},
    
        { pdi:'PDI01', name: 'Oslo politidistrikt', projectid: '21708-politidistriktene', email: 'post.oslo@politiet.no'},
        { pdi:'PDI02', name: 'Asker og Bærum politidistrikt', projectid: '21708-politidistriktene', email: 'post.askerogbaerum@politiet.no'},
        { pdi:'PDI03', name: 'Østfold politidistrikt', projectid: '21708-politidistriktene', email: 'post.ostfold@politiet.no'},
        { pdi:'PDI04', name: 'Follo politidistrikt', projectid: '21708-politidistriktene', email: 'post.follo@politiet.no'},
        { pdi:'PDI05', name: 'Romerike politidistrikt', projectid: '21708-politidistriktene', email: 'post.romerike@politiet.no'},
        { pdi:'PDI06', name: 'Hedmark politidistrikt', projectid: '21708-politidistriktene', email: 'post.hedmark@politiet.no'},
        { pdi:'PDI07', name: 'Gudbrandsdal politidistrikt', projectid: '21708-politidistriktene', email: 'post.gudbrandsdal@politiet.no'},
        { pdi:'PDI08', name: 'Vestoppland politidistrikt', projectid: '21708-politidistriktene', email: 'post.vestoppland@politiet.no'},
        { pdi:'PDI09', name: 'Nordre Buskerud politidistrikt', projectid: '21708-politidistriktene', email: 'nordre.buskerud@politiet.no'},

        { pdi:'PDI10', name: 'Søndre Buskerud politidistrikt', projectid: '21708-politidistriktene', email: 'post.sondre.buskerud@politiet.no'},
        { pdi:'PDI11', name: 'Vestfold politidistrikt', projectid: '21708-politidistriktene', email: 'post.vestfold@politiet.no'},
        { pdi:'PDI12', name: 'Telemark politidistrikt', projectid: '21708-politidistriktene', email: 'post.telemark@politiet.no'},
        { pdi:'PDI13', name: 'Agder politidistrikt', projectid: '21708-politidistriktene', email: 'post.agder@politiet.no'},
        { pdi:'PDI14', name: 'Rogaland politidistrikt', projectid: '21708-politidistriktene', email: 'post.rogaland@politiet.no'},
        { pdi:'PDI15', name: 'Haugaland og Sunnhordland politidistrikt', projectid: '21708-politidistriktene', email: 'post.hs@politiet.no'},
        { pdi:'PDI16', name: 'Hordaland politidistrikt', projectid: '21708-politidistriktene', email: 'post.hordaland@politiet.no'},
        { pdi:'PDI17', name: 'Sogn og Fjordane politidistrikt', projectid: '21708-politidistriktene', email: 'post.sfj@politiet.no'},
        { pdi:'PDI18', name: 'Sunnmøre politidistrikt', projectid: '21708-politidistriktene', email: 'post.sunnmore@politiet.no'},
        { pdi:'PDI19', name: 'Nordmøre og Romsdal politidistrikt', projectid: '21708-politidistriktene', email: 'post.noro@politiet.no'},
        { pdi:'PDI20', name: 'Sør-Trøndelag politidistrikt', projectid: '21708-politidistriktene', email: 'post.sor-trondelag@politiet.no'},
        { pdi:'PDI21', name: 'Nord-Trøndelag politidistrikt', projectid: '21708-politidistriktene', email: 'post.nord-trondelag@politiet.no'},
        { pdi:'PDI22', name: 'Helgeland politidistrikt', projectid: '21708-politidistriktene', email: 'post.helgeland@politiet.no'},
        { pdi:'PDI23', name: 'Salten politidistrikt', projectid: '21708-politidistriktene', email: 'post.salten@politiet.no'},
        { pdi:'PDI24', name: 'Midtre Hålogaland politidistrikt', projectid: '21708-politidistriktene', email: 'post.mhpd@politiet.no'},
        { pdi:'PDI25', name: 'Troms politidistrikt', projectid: '21708-politidistriktene', email: 'post.troms@politiet.no;'},
        { pdi:'PDI26', name: 'Vestfinnmark politidistrikt', projectid: '21708-politidistriktene', email: 'post.vestfinnmark@politiet.no'},
        { pdi:'PDI27', name: 'Østfinnmark politidistrikt', projectid: '21708-politidistriktene', email: 'post.ostfinnmark@politiet.no'}
      ];
      dm.isCurUni = isCurUni;
      dm.setCurUni = setCurUni;

      activate();

      function activate() {
        /*
          dm.query = 'Fædrelandsvennen';
          return getDocuments();
        */
         setCurUni( dm.unis[0] );
      }

      function getDocuments(){

        if (dm.query != '') {
          var qStr = dm.query;
          if (dm.curUni.pdi != "") { qStr += "%20title:"+  dm.curUni.pdi;} 
          dm.curQ = { querystring: qStr, timestamp: Date.now(), projectid: dm.projectid, resultpage: 1 };
          dm.queries.push(dm.curQ);
        }

          return docService.getAllDocs(dm.curQ).then(function(data){
 
              if (data.data.documents.length){ 
                dm.documents = data.data.documents;
                dm.totalDocs = data.data.total;
  
                angular.forEach(dm.documents, function(doc) {
                    doc.thmbUrl = doc.resources.page.image.replace("{size}", "thumbnail");
                    doc.pgUrl = doc.resources.page.image.replace("{size}", "large");
                    doc.displayTitle = doc.title; 
                    doc.curPage = 1;
                    doc.result = [];
                    doc.fullText = {};

                    doc.pid = doc.title.substr( 3, 2 ); // PDI01 - PDI28 format
                    doc.reqMail = dm.unis[parseInt(doc.pid, 10)].email;
                    
                  });

                // dm.query = '';
                setCurDoc( dm.documents[0] ); 
              } else {
                dm.documents = [];
                dm.totalDocs = 0;
                setCurDoc( dm.documents[0] );
              }

            return dm.documents;
          }) 
      }

      function getMoreDocuments(){

        var nextPage = dm.curQ.resultpage + 1;
        dm.curQ.resultpage = nextPage;

        return getDocuments();
      }

      function getCurrentDocument(){
        return docService.getCurDoc({ querystring: dm.query , timestamp: Date.now(), projectid: dm.projectid, resultpage: 1 }, dm.curDoc.id).then(function(data){

          if (data.data.results.length){
            dm.curDoc.result = data.data.results;
            setCurPage( dm.curDoc.result[0]);
          }
          return dm.curDoc;
        })
      }

      function getCurrentPage(){
               
        return docService.getCurPg(dm.curDoc).then(function(data){

          dm.curDoc.fullText = data;              
          dm.curDoc.curText = dm.curDoc.fullText.data;
          dm.curDoc.cases = [];

          var rxCase = new RegExp('20[\\d]{2}/[\\d]{1,5}-[\\d]{1,3}', 'g');
          var caseArray = dm.curDoc.curText.match(rxCase);

          if (caseArray !== null){

            var rxTitle = new RegExp('(?!<=Sak:[\\s]{2}[\\w]{+}[\\s]{1})([^:])+(?=Dok:)', 'g');
            var titleArray = []; 
            titleArray = dm.curDoc.curText.match(rxTitle);

            if (titleArray !== null){
              for (var i = 0; i < caseArray.length; i++) {dm.curDoc.cases.push({id: caseArray[i], title: titleArray[i]  }); }
            } else {
              for (var i = 0; i < caseArray.length; i++) {dm.curDoc.cases.push({id: caseArray[i], title: caseArray[i]  }); }
            }
          } else {
            dm.curDoc.cases.push({id: '', title: 'Søk om innsyn'  }); 
          }

          return dm.curPage;
        })

      }

      function setCurDoc(doc){
        dm.curDoc = doc;
        getCurrentDocument();
        return dm;
      }

      function isCurDoc(doc){
        return !!(dm.curDoc === doc);
      }

      function setCurPage(pg){
        dm.curDoc.curPage = pg;
        getCurrentPage();
        return dm.curDoc;
      }

      function isCurPage(pg){
        return !!(dm.curDoc.curPage === pg);
      }


      function setCurUni(uni){
  // todo: change to filter in query by name
  //      dm.projectid = uni.projectid;
        dm.curUni = uni;
        return dm.curUni;
      }

      function isCurUni(uni){
        return !!(dm.curUni === uni);
      }

    }
})();