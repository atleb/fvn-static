<?php 
  /**
  * Fædrelandsvennen Åpenhetsportal ("Sørlandsporten")
  * version: 4
  * page: politidistrikt
  * info: new version with alternate structure
  */
  $pgTitle = "Søk i postjournalen til alle politidistriktene";

  $incPath = '../../';

  include($incPath.'head.php');
  include($incPath.'menubrand.php');
?>
 
  <div class="container fullImg" id="ng-app" data-ng-app="app">

    <div class=""  data-ng-controller="docController as dm">

      <div class="intro row">
        <div class="col-sm-9"><h2>Søk i postjournalen til alle politidistiktene</h2>  
          <p>Ved hjelp av DocumentCloud har Fædrelandsvennen har gjort postlista søkbar tilbake til 2011.
          <p>Den første trefflisten viser postlister som inneholder søkeordet ditt. Markér et dokument, og sidene med søkeordet vil vises.Markér en av disse sidene, og den vises stort i feltet til høyre. </p>
          <p>Når du finner et dokument du vil bestille, merker du deg saksnummer og dokumentnummer. Klikk så på den tilhørende knappen og bestill.</p> 
        </div>
        <div class="col-sm-3">
            <h4>Mer informasjon knyttet til politiet:</h4>
            <ul class='uniLinks'>
              <li><a href="http://blogg.fvn.no/krim/" target="_blank">Håndbok i kriminaljournalistikk</a></li>
              <li>Bergens Tidendes reportasjeserie om  <a href="http://112.bt.no/" target="_blank">politiets prioriteringer og ressursbruk</a></li>
              <li><a href="http://112.bt.no/multimedia/lovbrudd/kart/" target="_blank">Statistikk lovbrudd 2002-2011</a></li>
              <li>VG spesial om <a href="http://www.vg.no/spesial/2013/politinorge/" target="_blank">politidistriktenefra 2013</a></li>
              
            </ul>

        </div>
      </div>

      <div class='row' >     

        <div class="buttonRows  col-sm-6">
          <h4>Velg fra kart</h4>
          ...jf http://112.bt.no/multimedia/lovbrudd/kart/ oppdeling?
          http://112.bt.no/multimedia/lovbrudd/kart/zones.js + GeoJson lib +?
          Norge
        </div>

        <div class='col-sm-12 '>
          <form ng-submit="dm.getDocuments()">
            <div class="buttonRows row col-sm-6">
            <h4>Velg fra liste over alle politidistriktene</h4>
            <ul class="uniList ">
              <li data-ng-repeat="uni in dm.unis" >
                <button class='btn' data-ng-class="{'btn-primary': dm.isCurUni(uni)}" data-ng-click="dm.setCurUni(uni)">
                   {{uni.name}}
                </button>
              </li>
            </ul>
            </div>
          
            <input type="text" class="fullSearch" ng-model="dm.query" placeholder="Eksempel: kripos" />
            <input type="submit" class="btn btn-lg btn-success"  value="Søk i postjournalen" />

            </form>
          <p><em>Søket fra Document Cloud støtter desverre ikke oppsettet til Internet Explorer, prøv en annen nettleser eller mobil.</em></p>
        </div>  
       
      </div><!--search row -->
      <div class='row resultsWrap ' ng-show='!dm.totalDocs && dm.queries.length'>
        <div class='col-sm-12'>
          <h4>Ingen treff for {{dm.query}} </h4>
        </div>
      </div>
      <div class='row resultsWrap ' ng-show='dm.totalDocs'>
        <div class='col-sm-12'>
          <h4>Treff i {{dm.totalDocs}} dokumenter</h4>
          <span class="resultPaginate">Viser treff {{(dm.documents.length * (dm.curQ.resultpage-1)) +1 }} - {{dm.documents.length * dm.curQ.resultpage }}</span>
          <ul class="resultList">
              <li data-ng-repeat="docu in dm.documents track by docu.id" >
                <button class='btn' data-ng-class="{'btn-primary': dm.isCurDoc(docu)}" data-ng-click="dm.setCurDoc(docu)">{{docu.displayTitle}}</button>
              </li>
          </ul>
         <button data-ng-click="dm.getMoreDocuments()" class="btn btn-sm btn-success">Hent neste {{dm.documents.length}} dokumenter med treff </button>
        </div>
      </div>

      <div class='row resultsWrap ' ng-show="dm.curDoc.curPage">
        <h1>Viser detaljer for valgt dokument: {{dm.curDoc.displayTitle}}</h1>
        <div class='col-sm-2'>
          <h4>Sider med treff</h4>
          <ul class='pageList'>
            <li data-ng-repeat="page in dm.curDoc.result" >
              <button class="btn" data-ng-class="{'btn-primary': dm.isCurPage(page)}" data-ng-click="dm.setCurPage(page)">
                <img ng-src='{{dm.curDoc.thmbUrl.replace("{page}", page)}}'><br>Side {{page}}
              </button>
            </li>
          </ul>
        </div>

        <div class='col-sm-10 pageWrap' >
          <h4>Be {{dm.curDoc.uni}} om innsyn:</h4>
          <ul class='caseList'>
            <li data-ng-repeat="case in dm.curDoc.cases" >
              <a class="btn btn-warning"  
              ng-href='mailto:{{dm.curDoc.reqMail}}?subject=Innsynsbegjæring&body=Søker%20om%20innsyn%20i%20saksnummer%20{{case.id}}%20-%20{{case.title}}'>
              {{case.id}}</a>
              {{case.title}}
            </li>
          </ul>
          <div class="pageImg">
            <img ng-src='{{dm.curDoc.pgUrl.replace("{page}", dm.curDoc.curPage)}}'>
          </div>
          <div class="pageText">
            <h6>Tekst fra siden: <em>(Maskinelt lest via Document Cloud)</em></h6>
            <pre>{{dm.curDoc.curText}}</pre>
          </div>
          
        </div>

      </div><!-- results -->

    </div><!-- dm -->
  </div> <!-- container app-->

  <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js'></script>
  <script type='text/javascript' >
   'use strict';

    var app = angular.module('app', [  ]);
    app.run(['$rootScope', function ($rootScope) {   }]);
  </script>

  <script type='text/javascript' src='docController.js'></script>
  <script type='text/javascript' src='docService.js'></script>

<?php 
  include($incPath.'stats.php');
  include($incPath.'footer.php');
?>