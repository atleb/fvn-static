<?php
  /**
  * Fædrelandsvennen Åpenhetsportal ("Sørlandsporten")
  * version: 3
  * page: footer menu fragment
  * info: information links and branding
  */
?>
<footer>
<div class="row">
<div class="col-md-6"> 
<ul class="legals">
        <li><a href="mailto:tips@fvn.no">Tips oss</a></li>
        <li><a href="mailto:tarjei.leer-salvesen@fvn.no">Varsle om feil</a></li>
        <li class=''><a href="http://www.fvn.no/lokalt/article2606584.ece">Få hjelp til innsyn</a></li>
        <li class=''><a href="http://www.fvn.no/lokalt/article2612669.ece">Derfor gjør vi det</a></li>
        <li class=''><a href="http://www.fvn.no/lokalt/article2651833.ece">Slik gjorde vi det</a></li>     
        <li class=''><a href="http://www.fvn.no/lokalt/article2606586.ece">Dine rettigheter</a></li>
</ul>
</div>
<div class="col-md-6 credit">
<p>En tjeneste fra <a href="http://www.fvn.no">Fædrelandsvennen</a>, ved hjelp av teknologien fra <a href="https://www.documentcloud.org"><img src="https://www.documentcloud.org/images/home/logo.png" ></a>
</p>
</div>
</div>
</footer>

</body>
</html>