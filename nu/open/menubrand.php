<?php
  /**
  * Fædrelandsvennen Åpenhetsportal ("Sørlandsporten")
  * version: 4
  * page: menu fragment
  * info: menu and logo branding, logo inline styled text
  * var: 
  */
?>
<section class="topp" id="topp">
    <div class="topp"><div class="row"><div class="col-md-6 pull-right"> 
        Utviklet av <a href="http://www.fvn.no">Fædrelandsvennen <span class="icon-f"></span></a>
    </div></div></div>
</section>
<div id="blow"></div>    
          
<section class="fvn_navn" id="fvn_navn">
    <div class="container">
        <div class="row">
        <div class="col-md-12  text-center">
            <div class=""><img src="/open/sorlandsporten.png"></div>
            <h1>SØRLANDS<span>PORTEN</span></h1>
            <a href="#note2" class="scroll_to_search_btn">DEN RASKESTE VEIEN TIL INNSYN I OFFENTLIGE DOKUMENTER</a>
        </div>
        </div>
    </div>
</section>