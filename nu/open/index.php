<?php 
  /**
  * Fædrelandsvennen Åpenhetsportal ("Sørlandsporten")
  * version: 4
  * page: frontpage
  * info: static overview page
  */
  $pgTitle = "Sørlandsporten";

  include('head.php');
  include('menubrand.php');
?>

<section class="punkter text-center" id="punkter">
    
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="punkter-padding">
    <div class="col-md-4"><a href="#note2">
        <div class="icon">
            <span class="pe pe-7s-note2"></span>
        </div>
        <h2>Offentlige Journaler</h2>
        <p>Her har vi samlet tilgang til postlistene for alle Agder-kommunene samt for viktige statlige institusjoner og kommunale foretak.</p>
    </a></div>
    <div class="col-md-4"><a href="http://www.fvn.no/lokalt/article2606586.ece">
        <div class="icon">
            <span class="pe pe-7s-look"></span>
        </div>
        <h2>Innsyn</h2>
        <p>Enhver kan be om innsyn i offentlige dokumenter. Det er gratis og enklere enn du tror. Les om dine rettigheter og få hjelp.</p>
    </a></div>
    <div class="col-md-4"><a href="#search">
        <div class="icon">
            <span class="pe pe-7s-search"></span>
        </div>
        <h2>Tilgjengelighet</h2>
        <p>Fædrelandsvennen har gjort postjournaler for Kristiansand kommune, Sørlandet Sykehus og norske universiteteter søkbare</p>
    </a></div>
    <div class="clearfix"></div>
    </div>
    </div>
</div>
</div>
</section>
    
<section class="searchLinks" id="search">
    <div class="container"><div class="row">
    <div class="col-md-12">
        <h1>
            <div class="icon"><span class="pe pe-7s-search"></span></div>
            Søkeløsninger satt opp av Fædrelandsvennen og gjort offentlig tilgjengelig
        </h1>
        <div class="col-md-4 col-xs-6">
            <a href="/open/kristiansand/" class="knapp ">Kristiansand kommune</a> 
            <a href="/open/havn/" class="knapp ">Kristiansand Havn</a>
        </div>
        <div class="col-md-4 col-xs-6">
            <a href="/open/sshf/" class="knapp ">Sørlandet Sykehus</a>
            <a href="/open/hso/" class="knapp ">Helse Sør Øst</a>
        </div>
        <div class="col-md-4 col-xs-6">
            <a href="/open/uia/" class="knapp ">Universitetet i Agder</a> 
            <a href="/open/universitet/" class="knapp ">alle norske universitet</a>
        </div>
    </div>
    </div></div>
</section>    
  
<section class="punkter clrs text-center" id="note2">
<div class="container">
    <h1><div class="icon"><span class="pe pe-7s-note2"></span></div>
        Status for digital tilgang på postlister hos kommunene i Agder-fylkene
    </h1>
<div class="row clr">
<div class="col col-md-4">
<div class="clr">
<h2><span>Søkbare med arkiv</span></h2>
<p>Disse kommunene har digitalt søkbare postlister og digitalt tilgjengelig arkiv og burde være forbilde for de øvrige kommunene i Agder.</p>
</div>
</div>
<div class="col col-md-4">
<div class="clr">
<h2><span>Søkbare</span></h2>
<p>Disse kommunene har søkbare postlister som fungerer rimelig bra. Knappene fører deg direkte til kommunenes egne sider.</p>
</div>
</div>
<div class="col col-md-4">
<div class="clr">
<h2><span>Uten søkemotor</span></h2>
<p>Disse kommunene har postlister lagt ut som tekst, men mangler søkemotor. Her kan det være lettere å søke via Google.</p>
</div>
</div>
</div>              
              
<div class="row" id="kommuner">
<div class="col-md-4">
    <a href="http://innsyn.birkenes.kommune.no/wfinnsyn.ashx?response=journalpost_postliste&amp;showresults=true" target="_blank" class="c928 knapp bra"><span>Birkenes</span></a>
    <a href="http://www.flekkefjord.kommune.no/lokaldemokratipolitikk-rad-og-utvalg/postliste" target="_blank" class="c1004 knapp bra"><span>Flekkefjord</span></a>
    <a href="http://lillesand.kommune.no/Innsyn/" target="_blank" class="c926 knapp bra"><span>Lillesand</span></a>
    <a href="http://innsyn.songdalen.kommune.no/wfinnsyn.ashx?response=journalpost_postliste" target="_blank" class="c1070 knapp bra"><span>Songdalen</span></a>
</div>  
<div class="col-md-4">
    <a href="http://www.arendal.kommune.no/Postliste/Postliste/#/16.04.2014/16.04.2014/" target="_blank" class="knapp ok c906"><span>Arendal</span></a>
    <a href="http://159.171.48.136/eInnsynBygland/" target="_blank" class="knapp ok c938"><span>Bygland</span></a>
    <a href="http://159.171.48.136/eInnsynBykle/" target="_blank" class="knapp ok c941"><span>Bykle</span></a>
    <a href="http://159.171.48.136/eInnsynEvje/" target="_blank" class="knapp ok c937"><span>Evje og Hornnes</span></a>
    <a href="https://login.farsund.kommune.no/einnsyn" class="knapp ok c1003 "><span>Farsund</span></a>
    <a href="http://www.froland.kommune.no/Postliste/" target="_blank" class="knapp ok c919"><span>Froland</span></a>
    <a href="http://159.171.0.170/einnsyn-GJE/" target="_blank" class="knapp ok c911"><span>Gjerstad</span></a>
    <a href="http://www.grimstad.kommune.no/Politikk/Postliste/" target="_blank" class="knapp ok c904"><span>Grimstad</span></a>
    <a href="http://159.171.48.136/eInnsynIveland/" target="_blank" class="knapp ok c935"><span>Iveland</span></a>
   <a href="http://krs-postliste.cloudapp.net/" target="_blank" class="knapp ok c1001"><span>Kristiansand</span></a>
    <a href="http://einnsyn.lyngdal.kommune.no/einnsyn/journalpost" target="_blank" class="knapp ok c1032" ><span>Lyngdal</span></a>
    <a href="http://159.171.0.170/einnsyn-RIS/" target="_blank" class="knapp ok c901"><span>Risør</span></a>
    <a href="http://212.37.255.195/eInnsyn/" target="new" class="knapp ok c1018"><span>Søgne</span></a>
    <a href="http://159.171.0.170/einnsyn-tve/" target="_blank" class="knapp ok c902"><span>Tvedestrand</span></a>
    <a href="http://159.171.48.136/eInnsynValle/" target="_blank" class="knapp ok c940"><span>Valle</span></a>
    <a href="http://159.171.0.170/einnsyn-VEG/" target="_blank" class="knapp ok c912"><span>Vegårshei</span></a>
    <a href="http://159.171.0.170/einnsyn-AML/" target="_blank" class="knapp ok c929"><span>Åmli</span></a>
</div>       

<div class="col-md-4">
    <a href="http://innsyn.audnedal.kommune.no/Publikum/Modules/Innsyn.aspx?mode=start" target="_blank" class="c1027 knapp nei"><span>Audnedal</span></a>
    <a href="http://innsyn.haegebostad.kommune.no/Publikum/Modules/Innsyn.aspx?mode=start" target="_blank" class="c1034 knapp nei"><span>Hægebostad</span></a>
    <a href="http://innsyn.kvinesdal.kommune.no/Publikum/Modules/innsyn.aspx?mode=start" target="_blank" class="c1037 knapp nei"><span>Kvinesdal</span></a>
    <a href="http://www.lindesnes.kommune.no/min-side/postlister" target="_blank" class="c1029 knapp nei"><span>Lindesnes</span></a>
    <a href="http://innsyn.mandal.kommune.no/Publikum/Modules/innsyn.aspx?mode=start" target="_blank" class="c1002 knapp nei"><span>Mandal</span></a>
    <a href="http://innsyn.marnardal.kommune.no/Publikum/Modules/innsyn.aspx?mode=pl&SelPanel=0&ObjectType=ePhorteRegistryEntry&VariantType=Innsyn&ViewType=Table&Query=RecordDate%3a%28-7%29+AND+DocumentType%3a%28I%2cU%29" target="_blank" class="c1021 knapp nei"><span>Marnardal</span></a>
    <a href="http://innsyn.sirdal.kommune.no/Publikum/Modules/innsyn.aspx?mode=pl&SelPanel=0&ObjectType=ePhorteRegistryEntry&VariantType=Innsyn&ViewType=Table&Query=RecordDate%3a(-7)+AND+DocumentType%3a(I%2cU)" target="_blank" class="c1046 knapp nei"><span>Sirdal</span></a>
    <a href="http://vennesla.kommune.no/ojournal/" target="_blank" class="c1014 knapp nei"><span>Vennesla</span></a>
    <a href="http://innsyn.aseral.kommune.no/Publikum/Modules/innsyn.aspx?mode=pl&SelPanel=0&ObjectType=ePhorteRegistryEntry&VariantType=Innsyn&ViewType=Table&Query=RecordDate:(-14)+AND+DocumentType:(I,U,N,X)" target="_blank" class="c1026 knapp nei"><span>Åseral</span></a>

</div>       

</div>
    
<h1 style="padding-top:80px;">Andre offentlige postlister</h1>
              
<div class="row" id="andre_linker">
<div class="col-md-4">
<a href="http://vaf.cloudapp.net/Journal/Search?expandSearch=1" target="_blank" class="knapp ok"><span>Vest-Agder<br>fylkeskommune</span></a>
<a href="http://www.ikava.no/sider/tekst.asp?side=207" target="_blank" class="knapp ok"><span>Interkommunalt<br>arkiv i Vest-Agder</span></a>
</div>       
<div class="col-md-4">
<a href="http://www.austagderfk.no/politikk/motekalender/postliste/" target="_blank" class="knapp ok"><span>Aust-Agder<br>fylkeskommune</span></a>
<a href="http://www.knutepunktsorlandet.no/fil.asp?FilkategoriId=89&back=1&MId1=248" target="_blank" class="knapp nei"><span>Knutepunkt<br>Sørlandet</span></a>
</div>       
<div class="col-md-4">
<a href="https://oep.no/search/advancedSearch.jsp" target="_blank" class="knapp ok"><span style="position:relative;top:-8px;">Departementer<br>direktorater<br>fylkesmenn (oep.no)</span></a>
</div>       
</div>   
    

    
</div>
</section>

<?php 
  include('stats.php');
  include('footer.php');
?>