<?php
  /**
  * Fædrelandsvennen Åpenhetsportal ("Sørlandsporten")
  * version: 4
  * page: header fragment
  * info: baseline for meta tags and icons
  * var: $pgTitle - meta title, string
  */
?>
<!DOCTYPE html>
<html lang="en" xmlns:ng="http://angularjs.org" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sørlandsporten - Fædrelandsvennen Åpenhetsportal</title>

    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <!-- baseline bootstrap, and project version CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/open/fvdoc-0.5.css">
</head>
<body>