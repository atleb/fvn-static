<?php

get_header(); ?>

	<div id="primary" class="content-area single">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'templates/content', 'single' ); ?>

			<?php single_post_navigation(); ?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php fvn_siste_saker( 'similar', get_the_category(), $post->ID ); ?>
<?php fvn_siste_saker( 'latest', get_the_category(), $post->ID ); ?>



<?php get_sidebar(); ?>
<?php get_footer(); ?>
