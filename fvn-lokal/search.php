<?php

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( '<div class="siste-saker-headline" style="color:#a0a0a0;">Søkeresultater for %s', 'fvn-lokal' ), '<span style="color:#303030;">' . get_search_query() . '</span></div>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				get_template_part( 'templates/content', 'search' );
				?>

			<?php endwhile; ?>

        <div style="margin-top:100px;border-top:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;"><?php pagination_nav(); ?></div>
            

		<?php else : ?>

			<?php get_template_part( 'templates/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
