<?php
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main index" role="main">

            
        
		<?php if ( have_posts() ) : ?>

        <div class="grid">

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
					get_template_part( 'templates/content', get_post_format() );
				?>
			<?php endwhile; ?>
			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'templates/content', 'none' ); ?>

        </div>    
            
		<?php endif; ?>

        <?php pagination_nav(); ?>    
        
            
            
            
		</main><!-- #main -->
	</div><!-- #primary -->

<?php /* get_sidebar(); */ ?>
<?php get_footer(); ?>
