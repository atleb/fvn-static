<?php


if ( ! function_exists( 'the_posts_navigation' ) ) :
function the_posts_navigation() {
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation posts-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Posts navigation', 'fvn-lokal' ); ?></h2>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( 'Older posts', 'fvn-lokal' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts', 'fvn-lokal' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;



if ( ! function_exists( 'single_post_navigation' ) ) :
function single_post_navigation() {
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Post navigation', 'fvn-lokal' ); ?></h2>
		<div class="next-prev grid">
			<?php
				next_post_link( '<div class="unit half">%link<span class="navicn ui-icon-angle-left"></span></div>', '%title' );
                //echo '<div class="unit one-fifth"></div>';
				previous_post_link( '<div class="unit half">%link<span class="navicn ui-icon-angle-right"></div></span>', '%title' );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;



if ( ! function_exists( 'fvn_lokal_posted_on' ) ) :
function fvn_lokal_posted_on($haspostformat) {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$byline = sprintf(
		_x( '%s', 'post author', 'fvn-lokal' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

    if ($haspostformat) { 
        echo '<span class="byline">' . $byline . '</span> skriver om idretten sin. <span class="posted-on">Publisert ' . $time_string . '</span>'; 
    } 
	else { echo 'Skrevet av <span class="byline">' . $byline . '</span><span class="posted-on"> ' . $time_string . '</span>'; }

}
endif;


if ( ! function_exists( 'fvn_lokal_entry_footer' ) ) :
function fvn_lokal_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		$categories_list = get_the_category_list( __( '|', 'fvn-lokal' ) );
		if ( $categories_list && fvn_lokal_categorized_blog() ) {
			printf( '<div class="cat-links"><span class="fvn-icon tag icon-tag"></span>' . __( '%1$s', 'fvn-lokal' ) . '</div>', $categories_list );
		}
		$tags_list = get_the_tag_list( '', __( '|', 'fvn-lokal' ) );
		if ( $tags_list ) {
			printf( '<div class="tag-links"><span class="fvn-icon tag icon-tag"></span>' . __( '%1$s', 'fvn-lokal' ) . '</div>', $tags_list );
		}
	}
	edit_post_link( __( 'Rediger Artikkelen', 'fvn-lokal' ), '<div class="edit-wrap"><span class="edit-link">', '</span></div>' );
}
endif;

if ( ! function_exists( 'the_archive_title' ) ) :
function the_archive_title( $before = '', $after = '' ) {
	if ( is_category() ) {
		$title = sprintf( __( 'Category: %s', 'fvn-lokal' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'Tag: %s', 'fvn-lokal' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'Author: %s', 'fvn-lokal' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'Year: %s', 'fvn-lokal' ), get_the_date( _x( 'Y', 'yearly archives date format', 'fvn-lokal' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'Month: %s', 'fvn-lokal' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'fvn-lokal' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'Day: %s', 'fvn-lokal' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'fvn-lokal' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'fvn-lokal' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'fvn-lokal' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'Archives: %s', 'fvn-lokal' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s', 'fvn-lokal' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives', 'fvn-lokal' );
	}

	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) :
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		echo $before . $description . $after;
	}
}
endif;

function fvn_lokal_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'fvn_lokal_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'fvn_lokal_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		return true;
	} else {
		return false;
	}
}

function fvn_lokal_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	delete_transient( 'fvn_lokal_categories' );
}
add_action( 'edit_category', 'fvn_lokal_category_transient_flusher' );
add_action( 'save_post',     'fvn_lokal_category_transient_flusher' );




if ( ! function_exists( 'fvn_social_buttons' ) ) :
function fvn_social_buttons() {
    
$idurl = explode('?', 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
$ID = url_to_postid($idurl[0]);    

$url = get_permalink($ID);
$data = json_decode(file_get_contents('http://graph.facebook.com/'.$url), true);    
$fb_shares = ( ( isset( $data['shares'] ) ) ? '<span class="arrow_box">'.( $data['shares'] ).'</span>' : '');
    
$face = 'href="http://www.facebook.com/sharer.php?u='.$url.'&t=Bra sak på @lokalsporten '.$url.' #fevennen"';
$tweet = 'href="http://twitter.com/home?status='.$url.' - Bra sak på @lokalsporten #fevennen"';
$share = '<a style="color:rgb(59, 89, 151);" data-popup="true" data-width="580" data-height="320" '.$face.' class="share-link share-link-facebook" title="Share on facebook">'.$fb_shares.'<span class="share-icon icon-facebook"></span>Del →</a><a style="color:rgb(65, 183, 216);" data-popup="true" '.$tweet.' class="share-link share-link-twitter" title="Share on twitter"><span class="share-icon icon-twitter"></span> Tweet →</a>';   
echo $share;
}
endif;


add_action( 'pre_hyphen_title', 'title_wordwrap' );
// wrapper alle tittelordene i en span for å legge in bindestrek/ orddeling
function title_wordwrap( $post_id )
{ 
$post_title = get_the_title($post_id);
return preg_replace('([a-zA-ZæøåÆØÅ.,!?0-9]+(?![^<]*>))', '<span>$0</span>', $post_title);
}


add_action( 'check_video_focus', 'sjekk_video_fokus' );
function sjekk_video_fokus( $post_id )
{ 
    if ( get_post_meta($post_id, 'meta-videofokus', true) ) {
        //if (get_post_meta($post_id)['meta-videofokus'][0])
            echo '<span class="video-fokus"><span></span></span>';
    }
}

add_action( 'fvn_latest_roller', 'fvn_siste_saker' );
function fvn_siste_saker( $type, $cats, $currentId) {
    
$posts_count = 9;  
global $wp_query;
$temp_query = $wp_query;
$the_query = new WP_Query("post_type=post&posts_per_page=".$posts_count."&orderby=date&order=ASC");

if ($type == 'similar') {
    $catlist = "";
    forEach( $cats as $c ) {
        if( $catlist != "" ) { $catlist .= ","; }
            $catlist .= $c->cat_ID;
    }
    $the_query = "posts_per_page=".$posts_count."&cat=" . $catlist;
}
    
    echo '<div id="rolled_'.$type.'" class="rolled">';
    echo '<div class="siste-saker-headline"><span class="fvn-icon tag icon-tag"></span>Siste saker'.( $type == 'similar' ? ' innen ' : '' );
    if ($type == 'similar') echo the_category(', ');
    echo '</div><div class="roller-viewport">';
    echo '<div class="roller-canister">';
        query_posts( $the_query );
        $count = 0;
        if (have_posts()) {
        while (have_posts()) {
            the_post();
            if( $count<$posts_count) {
                $class = '_'.(($count % 3) + 1);
                $count++;
                echo '<div class="roller-item '.$class.'"><header class="entry-header">';
            
                $src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); 
                $style = ( ($src[0]) ? 'style="background:url('.$src[0].');"' : '');   
                $link = get_the_permalink();
                $metas = get_post_meta(get_the_ID(), 'meta-videofokus', true);
                
                echo '<a href="'.$link.'">';
                echo '<div '.$style.' class="grid-image">';                                   
                if ( $metas ) { echo '<span class="video-fokus"><span></span></span>'; }
                echo '</div></a>';
                the_title( sprintf( '<h1 class="entry-title front roller"><a class="front" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); 
                echo '</header></div>';                         
        }}}
        $wp_query = $temp_query;
    
        while ($wp_query->have_posts()) : $wp_query->the_post();
        endwhile;
    echo '</div></div></div>';

    
}
