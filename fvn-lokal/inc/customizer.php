<?php

function fvn_lokal_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
}
add_action( 'customize_register', 'fvn_lokal_customize_register' );

function fvn_lokal_customize_preview_js() {
	wp_enqueue_script( 'fvn_lokal_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'fvn_lokal_customize_preview_js' );
