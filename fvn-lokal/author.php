<?php

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main index" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
                    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
					echo '<h1 class="siste-saker-headline archive"><span class="fvn-icon tag icon-tag"></span>'.$curauth->display_name.'</h1>';
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

            
			<?php /* Start the Loop */ ?>
        <div class="grid">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'templates/content', get_post_format() ); ?>
			<?php endwhile; ?>
        </div>
        <?php pagination_nav(); ?>    

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
