            </div><!-- #content -->
        </div>
    </div>

    <div class="footer-area full">
        <div class="main-page">
            <footer id="colophon" class="site-footer inner" role="contentinfo">
                <div class="site-info">
                    Lokalsporten - en tjeneste fra <a href="http://www.fvn.no" rel="designer">Fædrelandsvennen</a>                
                </div><!-- .site-info -->
            </footer><!-- #colophon -->
        </div>
    </div>

<?php wp_footer(); ?>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
var gaPath = document.location.pathname;
 ga('create', 'UA-10757236-1', 'fvn.no');
 ga('set', 'anonymizeIp', true);
ga('set', 'dimension2',  'article_open'); 
ga('set', 'contentGroup1',  'article_open');
ga('set', 'page', '/lokalsporten' +gaPath.substr(0,gaPath.length-1) + '.html' );
 ga('send', 'pageview');
</script>

</body>
</html>

