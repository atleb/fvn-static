<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<script type="text/javascript"> jQuery(document).ready(function($) { fvn_lokal_js(); }); </script>    
</head>
<body <?php body_class(); ?>>
   
    <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'fvn-lokal' ); ?></a>
    <div class="header-area full">
        <div class="main-page">
            <header id="masthead" class="site-header inner" role="banner">
                <div class="site-branding grid">
                    <div class="unit one-third">
                    <h1 class="site-title"><a class="fvn-icon logo icon-fvn-s" href="http://www.fvn.no"></a><a class="logo text anim" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                    </div>
                    <div class="unit two-thirds">
                        <nav id="site-navigation" class="main-navigation" role="navigation">
                            <span class="fvn-icon menu icon-menu-3 menu-toggle" aria-controls="menu" aria-expanded="false"></span>
                            <span id="meny-search">
                                <?php get_search_form(); ?>
                            </span>
                            <div class="fvn_hovedmeny"><?php wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => 'fvn_hovedmeny' ) ); ?></div>
                            <span id="search-form-icon" class="fvn-icon search icon-search"></span>
                        </nav><!-- #site-navigation -->  
                    </div>
                    
                    <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                </div>
            </header>
        </div>
    </div>
            
    <div class="fvn-info full init">
        <div class="inner">
            <div id="brukerskapt" class="brukerskapt">
                <div class="info"><div class="tip"></div>
                    <span class="kort">Brukerskapt innhold</span><span class="info-icon open ui-icon-info-circle"></span><span class="info-icon close ui-icon-close-circle hidden"></span>
                    <span class="lang hidden">Bli med i lokalsporten! På lokalsporten.fvn.no lar vi klubber og utøvere selv skrive fra stevner, trening og annet. Formålet er å speile mangfoldet og bredden i lokalidretten, og flere av sakene publiseres også på forsiden til fvn.no. Minst en gang i uka samler vi utvalgte bidrag i papiravisen. Vil din klubb være med som en av stadig flere bidragsytere, kan du sende en e-post til <a href="mailto:rune.stensland@fvn.no">Rune Stensland</a></span>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-area full">
        <div class="main-page">
                <div class="ad-wrapper aligncenter">
                <?php if ( wp_is_mobile()  ) { ?>

                <!--Mobile JavaScript Tag // Tag for network 1582: Schibsted - NO  // Website: SB_Faedrelandsvennen_Mobile // Page: Sport // Placement: TopBoard (5509470) // created at: Feb 27, 2015 8:40:37 AM-->
                <script language="javascript"><!--
                document.write('<scr'+'ipt language="javascript1.1" src="http://a.adtech.de/addyn/3.0/1582.1/0/0/-1/ADTECH;loc=100;grp=[group];alias=SB_Faedrelandsvennen_Mobile:Sport:TopBoard;misc='+new Date().getTime()+'"></scri'+'pt>');
                //-->
                </script><noscript><a href="http://core.adtech.de/adlink/3.0/1582.1/0/0/-1/ADTECH;loc=300;grp=[group];alias=SB_Faedrelandsvennen_Mobile:Sport:TopBoard;random=[insert random 20 character token here]">
                <img src="http://core.adtech.de/adserv/3.0/1582.1/0/0/-1/ADTECH;loc=300;grp=[group];alias=SB_Faedrelandsvennen_Mobile:Sport:TopBoard;random=[insert random 20 character token here]"
                border="0"></a></noscript>
                <!-- End of Mobile JavaScript Tag --> 

                <?php } else { ?>

                <!--JavaScript Tag // Tag for network 1582.1: Schibsted - NO  // Website: SB_Faedrelandsvennen_Desktop // Page: Blogg // Placement: TopBoard (5509062) // created at: Feb 27, 2015 8:31:21 AM-->
                <script language="javascript"><!--
                document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn|3.0|1582.1|5509062|0|1744|ADTECH;cookie=info;alias=SB_Faedrelandsvennen_Desktop:Blogg:TopBoard;loc=100;target=_blank;key=key1+key2+key3+key4;grp=[group];misc='+new Date().getTime()+'"></scri'+'pt>');
                //-->
                </script><noscript><a href="http://adserver.adtech.de/adlink|3.0|1582.1|5509062|0|1744|ADTECH;loc=300;alias=SB_Faedrelandsvennen_Desktop:Blogg:TopBoard;key=key1+key2+key3+key4;grp=[group];cookie=info;" target="_blank"><img src="http://adserver.adtech.de/adserv|3.0|1582.1|5509062|0|1744|ADTECH;loc=300;alias=SB_Faedrelandsvennen_Desktop:Blogg:TopBoard;key=key1+key2+key3+key4;grp=[group];cookie=info;" border="0" width="980" height="150"></a></noscript>
                <!-- End of JavaScript Tag -->               

                <?php } ?>
                </div>            

            
            <div id="content" class="site-content inner">
                

                
