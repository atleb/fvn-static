<?php


if ( ! isset( $content_width ) ) {
	$content_width = 720; /* pixels */
}

if ( ! function_exists( 'fvn_lokal_setup' ) ) :

function fvn_lokal_setup() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => __( 'fvn_hovedmeny', 'fvn-lokal' ),
	) );
    add_theme_support( 'html5', array(
		'search-form', 'gallery', 'caption',
	) );
	add_theme_support( 'post-formats', array(
		'aside',
	) );
}
endif; // fvn_lokal_setup
add_action( 'after_setup_theme', 'fvn_lokal_setup' );

// Endrer navn på Post Format Aside (Notis på norsk) til Blogg
function rename_post_formats( $safe_text ) {
    if ( $safe_text == 'Aside' || $safe_text == 'Notis' )
        return 'Blogg';
    return $safe_text;
}
add_filter( 'esc_html', 'rename_post_formats' );



/* ----------------------------------------------------------------- */
// Custom Checkbox for videofokus
/* ----------------------------------------------------------------- */

function custom_meta_box_markup($object)
{
wp_nonce_field(basename(__FILE__), "meta-box-nonce");
?> <div> <?php
            $checkbox_value = get_post_meta($object->ID, "meta-videofokus", true);
            if($checkbox_value == "")
            { ?>
                <input name="meta-videofokus" type="checkbox" value="true">
            <?php }
            else if($checkbox_value == "true")
            { ?>  
                <input name="meta-videofokus" type="checkbox" value="true" checked>
            <?php } ?>
        <span>Fokuser på videoinnhold</span>
</div> <?php  
}

function add_custom_meta_box()
{
    add_meta_box("demo-meta-box", "Ekstravalg", "custom_meta_box_markup", "post", "side", "high", null);
}
add_action("add_meta_boxes", "add_custom_meta_box");

function save_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;
    if(!current_user_can("edit_post", $post_id))
        return $post_id;
    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
    $slug = "post";
    if($slug != $post->post_type)
        return $post_id;
    $meta_box_checkbox_value = "";
    if(isset($_POST["meta-videofokus"]))
    {
        $meta_box_checkbox_value = $_POST["meta-videofokus"];
    }   
    update_post_meta($post_id, "meta-videofokus", $meta_box_checkbox_value);
}
add_action("save_post", "save_custom_meta_box", 10, 3);

/* ----------------------------------------------------------------- */



function fvn_hovedmeny()
{ 

$super_categories_ID = array(); // Kategorier uten parent
$parent_IDs = array(); // Alle parent IDs
$categories = get_categories(); 
foreach($categories as $category){
if ($category->category_parent == 0)
    $super_categories_ID[] = $category->cat_ID;
    $parent_IDs[] = $category->category_parent;
}
$super_categories_with_children = array(); // Kategorier uten parent OG uten children
foreach($super_categories_ID as $super_ID){
    if (! in_array($super_ID, $parent_IDs)) {
        $super_categories_with_children[] = $super_ID;
    }    
}
$super_categories_with_children[] = 1; // Fjerner [Ukategorisert]  
$excludes = implode(',', $super_categories_with_children);
$args = array(
'exclude'            => $excludes,
'title_li'           => __( '' ),
);
wp_list_categories( $args ); 
 
    
}

/*
function fvn_filter_the_title( $title ) {
// wrapper alle tittelordene i en span for å legge in bindestrek/ orddeling
return preg_replace('([a-zA-Z.,!?0-9]+(?![^<]*>))', '<span>$0</span>', $title);
}
add_filter( 'the_title', 'fvn_filter_the_title' );
*/









function jpb_unregister_widgets(){
  unregister_widget('WP_Widget_Pages');
  unregister_widget('WP_Widget_Calendar');
  unregister_widget('WP_Widget_Archives');
  unregister_widget('WP_Widget_Links');
  unregister_widget('WP_Widget_Meta');
  //unregister_widget('WP_Widget_Search');
  unregister_widget('WP_Widget_Text');
  unregister_widget('WP_Widget_Categories');
  unregister_widget('WP_Widget_Recent_Posts');
  unregister_widget('WP_Widget_Recent_Comments');
  unregister_widget('WP_Widget_RSS');
  unregister_widget('WP_Widget_Tag_Cloud');
  unregister_widget('WP_Nav_Menu_Widget');
}

add_action( 'widgets_init', 'jpb_unregister_widgets' );


function set_posts_per_page( $query ) {
  global $wp_the_query;
  //if ( ( ! is_admin() ) && ( $query === $wp_the_query ) && ( $query->is_search() ) ) {
  //elseif ( ( ! is_admin() ) && ( $query === $wp_the_query ) && ( $query->is_archive() ) ) {
  $query->set( 'posts_per_page', 12 );
  return $query;
}
add_action( 'pre_get_posts',  'set_posts_per_page'  );



function pagination_nav() {
    global $wp_query;
    $total_pages = $wp_query->max_num_pages;
 
    if ( $total_pages > 1 ) { 
        $current_page = max(1, get_query_var('paged')); ?>
        <nav class="pagination grid" role="navigation">
            <div class="nav-next unit one-third align-left"><?php previous_posts_link( '&larr; Nyere artikler' ); ?></div>
            <div class="nav-numbers unit one-third align-center">
            <?php echo paginate_links(array(
                'base' => get_pagenum_link(1) . '%_%',
                'format' => '?paged=%#%',
                'current' => $current_page,
                'total' => $total_pages,
                'mid_size' => 5,
                'prev_next' => FALSE,
            )); ?> 
            </div>
            <div class="nav-previous unit one-third align-right"><?php next_posts_link( 'Eldre artikler &rarr; ' ); ?></div>
            
        </nav>
<?php }
}


function fvn_lokal_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'fvn-lokal' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<span class="widget-title">',
		'after_title'   => '</span>',
	) );
}
add_action( 'widgets_init', 'fvn_lokal_widgets_init' );


function fvn_lokal_scripts() {
    wp_enqueue_style( 'fvn-lokal-fvn-icons', get_template_directory_uri() . '/css/fvn-icons.css',  array(), null, 'screen' );
	wp_enqueue_style( 'fvn-lokal-style', get_stylesheet_uri() );
    wp_enqueue_style( 'fvn-lokal-gridism', get_template_directory_uri() . '/css/gridism.css' ,  array(), null, 'screen' );
    wp_enqueue_style( 'fvn-uiicons-css', get_template_directory_uri() . '/css/ui-icons.css' ,  array(), null, 'screen' );
    wp_enqueue_style( 'fvn-gallery-css', get_stylesheet_directory_uri() . '/css/swipebox.css' ,  array(), null, 'screen' );   
    wp_enqueue_style( 'fvn-roller-css', get_stylesheet_directory_uri() . '/css/jquery.fs.roller.min.css' ,  array(), null, 'screen' );  

	wp_enqueue_script( 'fvn-lokal-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

    wp_enqueue_script( 'fvn-lokal-jquery', get_template_directory_uri() . '/js/jquery-1.11.2.min.js', array(), null, null );
    wp_enqueue_script( 'fvn-jquery-swipebox', get_stylesheet_directory_uri() . '/js/jquery.swipebox.js', array( 'fvn-lokal-jquery' ), null, null );
    wp_enqueue_script( 'fvn-jquery-fitvids', get_stylesheet_directory_uri() . '/js/jquery.fitvids.js', array( 'fvn-lokal-jquery' ), null, null );
    wp_enqueue_script( 'fvn-jquery-roller', get_stylesheet_directory_uri() . '/js/jquery.fs.roller.min.js', array( 'fvn-lokal-jquery' ), null, null );
    wp_enqueue_script( 'fvn-jquery-lokal', get_stylesheet_directory_uri() . '/js/fvn-lokal.js', array( 'fvn-lokal-jquery' ), null, null );
    
    

}
add_action( 'wp_enqueue_scripts', 'fvn_lokal_scripts' );


require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/customizer.php';




