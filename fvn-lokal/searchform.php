<?php
/*
Template Name: Search Page
*/
//  <input type="search" placeholder="Søk i Lokalsporten" name="search" id="search-form-input" class="search-form-input hidden" onkeyup="buttonUp();" required>

?>
	<form role="search" action="<?php echo site_url('/'); ?>" method="get" style="display:inline-block;">
		<input type="search" id="search-form-input" class="search-form-input hidden" name="s" placeholder="Søk i <?php echo bloginfo('name'); ?>" />
		<input type="submit" class="hidden" id="searchsubmit" alt="Search" value="Search" />
	</form>
