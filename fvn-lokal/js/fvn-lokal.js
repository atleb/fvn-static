(function($){
fvn_lokal_js = function() {

    
docCookies={getItem:function(sKey){if(!sKey){return null;}
return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*"+encodeURIComponent(sKey).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=\\s*([^;]*).*$)|^.*$"),"$1"))||null;},setItem:function(sKey,sValue,vEnd,sPath,sDomain,bSecure){if(!sKey||/^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)){return false;}
var sExpires="";if(vEnd){switch(vEnd.constructor){case Number:sExpires=vEnd===Infinity?"; expires=Fri, 31 Dec 9999 23:59:59 GMT":"; max-age="+vEnd;break;case String:sExpires="; expires="+vEnd;break;case Date:sExpires="; expires="+vEnd.toUTCString();break;}}
document.cookie=encodeURIComponent(sKey)+"="+encodeURIComponent(sValue)+sExpires+(sDomain?"; domain="+sDomain:"")+(sPath?"; path="+sPath:"")+(bSecure?"; secure":"");return true;},removeItem:function(sKey,sPath,sDomain){if(!this.hasItem(sKey)){return false;}
document.cookie=encodeURIComponent(sKey)+"=; expires=Thu, 01 Jan 1970 00:00:00 GMT"+(sDomain?"; domain="+sDomain:"")+(sPath?"; path="+sPath:"");return true;},hasItem:function(sKey){if(!sKey){return false;}
return(new RegExp("(?:^|;\\s*)"+encodeURIComponent(sKey).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=")).test(document.cookie);},keys:function(){var aKeys=document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g,"").split(/\s*(?:\=[^;]*)?;\s*/);for(var nLen=aKeys.length,nIdx=0;nIdx<nLen;nIdx++){aKeys[nIdx]=decodeURIComponent(aKeys[nIdx]);}
return aKeys;}};
    
    
// Funksjon som legger inn bindestrek i mobil-overskrifter for å unngå ødelagt layout eller orddeling av typen "Statsministe-r"
$.fn.hyph = function(){
var boxw = this.width();
this.find('span').each(function(){
if ( $(this).width() > boxw ) {
    var el = $(this).text();
    var letterw = $(this).width() / ( el.length - 1 );
    var pos = (el.length - boxw/letterw) > 3 ? boxw/letterw : el.length * 0.5; 
    var output = [el.slice(0, pos), '&shy;', el.slice(pos)].join('');
    $(this).html( output );
}
});   
this.css('opacity','1');
}; //fn     

    
// Bygger galleri + gjør alle bilder tilgjengelige for Swipebox
if ( $('.main-content-area').find('.gallery-item').length ) {
    $( '.gallery-item:first' ).addClass('gallery-entry');
    $( '.gallery-item:not(.gallery-entry)' ).hide();
    $('.gallery-item').each(function(index){
        var el = $(this);
        var orig_img = $(this).find('img').attr('src').replace(/[-]\d{3}[x]\d{3}/g, '');
        $(this).find('img').remove();
        $(this).find('a').attr('href', orig_img );
            if ( $(this).find('.wp-caption-text').length ) {
                el.find('a').attr('title', $(this).find('.wp-caption-text').text() + '('+(index+1)+'/'+$('.gallery-item').length+')' );
            }
    });
    $( '.gallery-item:first-child .gallery-icon' ).append('<div class="galleryicon"><span class="txt">'+$('.gallery-item').length+' bilder<span class="icn ui-icon-layers"></span></span></div>');
}
$('.gallery-entry a').css('background-image','url("'+$('.gallery-entry').find('a').attr('href')+'")');

$('a[href]').filter(function() {
    return /(jpg|gif|png)$/.test( $(this).attr('href'))
}).addClass('images');
$('.images').swipebox();

    
// Søkefeltfunksjonalitet
$('#search-form-icon').on('click', function(){
    if ( $('#search-form-input').val() ) {
        $('#searchsubmit').click();
    }
    $('.fvn_hovedmeny').toggleClass('hidden');
    $(this).toggleClass('active');
    $('#search-form-input').toggleClass('hidden');
    if ( $('#search-form-input').is(':visible') ) $('#search-form-input').focus();
});


// All Things Infoboble/ Brukerskapt
if ( ! docCookies.getItem('info_lest')  ) { $('.fvn-info.full').removeClass('init'); $('.site-title a.logo.text').removeClass('anim'); }

var outTimeout = setTimeout(function(){});
$('body').on('mouseover mousedown mouseout', '.fvn-info', function(e){
if (e.type == 'mouseover' ) {
    clearTimeout(outTimeout);
}
if (e.type == 'mousedown' ) {
    docCookies.setItem('info_lest', 'true', 3600);

    if ( ! $(e.target).is('a') ) {
        var tooltiptip = $('.brukerskapt div.info').outerWidth(true);
        $('.brukerskapt div.info').toggleClass('clicked').css('padding','2em 1em');
        if ( $('.brukerskapt div.info').hasClass('clicked') ) {
            $('.brukerskapt div.info span.lang').removeClass('hidden').show();
            $('.brukerskapt div.info span.info-icon.close').removeClass('hidden');
            $('.brukerskapt div.info span.info-icon.open').addClass('hidden');
            $('.brukerskapt div.tip').css('left', (tooltiptip/2)+'px');
        }
        else { 
            $('#masthead').click(); 
        }
    } // not home button
    else {
        window.location.href = $('.site-title a.logo.text').attr('href');              
    }
}//mousedown
if (e.type == 'mouseout' ) {
    outTimeout = setTimeout( function() {
    if ( (! $('.brukerskapt').hasClass('closed')) && ( docCookies.getItem('info_lest')  )  ) {
            $('#masthead').click(); 
        }
    }, 500);    
}
});

$('#masthead').on('click',function(e) {
    if ( $(e.target).is('nav, div, header') ) {

            $('.site-title a.logo.text').toggleClass('anim');
            $('.brukerskapt div.info').toggleClass('anim');

            if ( $('.fvn-info.full').hasClass('init')  ) {
            $('.fvn-info.full').css('transition-delay','0s');
            $('.fvn-info.full').removeClass('init');
            $('.site-title a.logo.text').removeClass('anim');
            $('.brukerskapt div.info').removeClass('anim');
            }                

            if ( ! $('.brukerskapt div.info span.lang').hasClass('hidden') ) $('.brukerskapt div.info').removeClass('clicked');
            if ( $('.brukerskapt div.info').hasClass('anim') ) {
                $('.brukerskapt div.info span.lang').hide();
                $('.brukerskapt').addClass('closed').fadeOut(350);
            } else {
                $('.brukerskapt div.info').removeClass('clicked').css('padding','1em 2em');
                $('.brukerskapt div.info span.kort').add('.brukerskapt div.info span.info-icon.open').removeClass('hidden');
                $('.brukerskapt div.info span.info-icon.close').addClass('hidden');
                $('.brukerskapt').removeClass('closed').fadeIn(500);        
            }
    }
});

$('.site-title a.logo.text').on('touchstart mouseover mouseout mousedown',function(e){
e.preventDefault();
var touched = false;    
if ( e.type == 'touchstart' && $(this).hasClass('anim') ) {
    touched = true;
    clearTimeout(outTimeout);
    if ( $(this).hasClass('anim') ) {
        $('#masthead').click(); 
    }
}
if (e.type == 'mouseover') {
    clearTimeout(outTimeout);
    if ( $(this).hasClass('anim') ) {
        $('#masthead').click(); 
    }
}
if (e.type == 'mouseout' ) {
    outTimeout = setTimeout( function() {
    if ( (! $('.brukerskapt').hasClass('closed')) && ( docCookies.getItem('info_lest')  )  ) {
        $('#masthead').click(); 
        }
    }, 1000);
} //if
if ( (e.type == 'mousedown') || (e.type == 'touchstart' && !touched) ) {
    console.log('zz '+touched);
    window.location.href = $(this).attr('href');            
}
});

$(window).scroll(function(){
var aTop = $('.header-area').height();
if ( $(this).scrollTop() >= 5 ) {
    if ( ! $('.brukerskapt').hasClass('closed') ) $('#masthead').click(); 
}
});
    
    
// Mobilmeny
$('.menu-toggle').click( function(e) {
$('#site-navigation').toggleClass('toggled');
    
if ( $('#site-navigation').hasClass('toggled') ) {
    $('<div id="mobilmeny-content" />').appendTo( $('#masthead') );
    $('<div id="mobilmeny-overlay" />').appendTo('body');
    $('.fvn-info.full').hide();    

    $('#mobilmeny-content').html( $('#meny-search').html() );
    $('.fvn_hovedmeny > li > ul').each( function(){
    $('#mobilmeny-content').append( '<div class="mainmenu"><span>'+$(this).parent().find('a:first').text()+'</span><div class="submenu hidden">'+$(this).filter( function(){ return $(this).find('li').attr('class','') }).html()+'<div></div>' );
    }); 
    $('#mobilmeny-content').append( '<div class="mainmenu"><span><a href="'+$('.site-title a.logo.text').attr('href')+'">Forside</a></span></div>' );
    
    $('#mobilmeny-content .search-form-input').removeClass('hidden');

    $('#mobilmeny-content .mainmenu').bind('click', function(){
        $(this).toggleClass('expanded');
        $(this).find('.submenu').toggleClass('hidden');
    });

}
else {
    $('#mobilmeny-overlay').remove();
    $('.fvn-info.full').show();    
    $('#mobilmeny-content .mainmenu').unbind('click');
    $('#mobilmeny-content').remove();
}

});    

    
    
// Initier Div    
if ( $('.entry-title.single').length ) $('.entry-title.single').hyph();
if ( $('.entry-title.search').length ) $('.entry-title.search').hyph();
$('.main-content-area').fitVids();
$('#rolled_similar').roller();        
$('#rolled_latest').roller();    
      
};
})(jQuery);

