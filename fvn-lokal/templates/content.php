<?php
/**
 * @package fvn-lokal
 */
?>

<?php
$c = $wp_query->current_post;
$class = '_'.(($c % 3) + 1);
?>

<div class="unit one-third <?php echo $class; ?>">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
        
    <?php 
    $src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); 
    $style = 'style="background:url('.$src[0].');"';   
    ?>   
    <a href="<?php echo get_permalink($post->ID) ?>">
    <div <?php if ($src[0]) { echo $style; } ?> class="grid-image">                                   
    <?php sjekk_video_fokus($post->ID); ?>     
    </div>    
    </a>
    <?php the_title( sprintf( '<h1 class="entry-title front"><a class="front" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
    
	</header><!-- .entry-header -->

	<footer class="entry-footer">
		<?php 
            //fvn_lokal_entry_footer(); 
        ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
</div>


