<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package fvn-lokal

$haspostformat = (has_post_format( 'aside' ) ? 'blogg' : false);
<?php fvn_lokal_posted_on($haspostformat); ?>

*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header readable-width">
        <?php echo sprintf('<h1 class="entry-title search"><a href="%s">'.title_wordwrap($post->ID).'</a></h1>', esc_url( get_permalink() ) ); ?>
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary readable-width">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer readable-width">
		<?php fvn_lokal_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
