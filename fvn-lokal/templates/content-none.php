<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package fvn-lokal
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="entry-title search"><?php _e( 'Ingen treff', 'fvn-lokal' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
    <div class="entry-summary align-center">		
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Klar til å skrive et innlegg? <a href="%1$s">Begynn her</a>.', 'fvn-lokal' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Prøv med andre søkeord', 'fvn-lokal' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'fvn-lokal' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
    </div>	        
	</div><!-- .page-content -->
    <?php fvn_siste_saker( 'latest', null, null ); ?>
    
</section><!-- .no-results -->
