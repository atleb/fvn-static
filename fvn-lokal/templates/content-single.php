<?php
/**
 * @package fvn-lokal
 */

// content-single.php - hentes av single.php

$haspostformat = (has_post_format( 'aside' ) ? 'blogg' : false);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php 
    $src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); 
    $style = 'style="background:url('.$src[0].');"';   
    ?>       

    <a href="<?php echo $src[0]; ?>">
    <div <?php if ($src[0]) { echo $style; } ?> class="featured-image">
    </div></a>

	<header class="entry-header readable-width <?php echo $haspostformat; ?>">
            <?php 
            if ( $haspostformat ) { echo '<h1 class="entry-title single blogg">'.title_wordwrap($post->ID).'</h1>'; }
            else { echo '<h1 class="entry-title single">'.title_wordwrap($post->ID).'</h1>'; } 
            ?>
		<div class="entry-meta">
		  <?php fvn_lokal_posted_on($haspostformat); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
    
<div class="share-buttons small aligncenter"><?php fvn_social_buttons(); ?></div>

    
    
	<div class="entry-content readable-width">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'fvn-lokal' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

    
	<footer class="entry-footer readable-width single">
		<?php fvn_lokal_entry_footer(); ?>
	</footer><!-- .entry-footer -->
    
    <div class="share-buttons big aligncenter"><?php fvn_social_buttons(); ?></div>
    
</article><!-- #post-## -->
