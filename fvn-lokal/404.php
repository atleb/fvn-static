<?php
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="entry-title search"><?php _e( 'Ukjent side', 'fvn-lokal' ); ?></h1>
                </header>

                
                
				<div class="page-content">
                    <div class="entry-summary align-center">URL eksisterer ikke</div>
				</div><!-- .page-content -->
                
                <br>
                <?php fvn_siste_saker( 'latest', null, null ); ?>
					<?php if ( fvn_lokal_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>

					<?php endif; ?>

                
                
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
