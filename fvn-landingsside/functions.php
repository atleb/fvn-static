<?php


function fvn_lokal_scripts() {
//  wp_enqueue_style( 'fvn-lokal-fvn-icons', get_template_directory_uri() . '/css/fvn-icons.css',  array(), null, 'screen' );
	wp_enqueue_style( 'fvn-lokal-style', get_stylesheet_uri() );

/*	wp_enqueue_script( 'fvn-lokal-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'fvn-lokal-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

  wp_enqueue_script( 'fvn-lokal-jquery', get_template_directory_uri() . '/js/jquery-1.11.2.min.js', array(), '20150222', true );
  wp_enqueue_script( 'fvn-jquery-roller', get_stylesheet_directory_uri() . '/js/jquery.fs.roller.min.js', array( 'fvn-lokal-jquery' ) );
 */   
}
add_action( 'wp_enqueue_scripts', 'fvn_lokal_scripts' );

/*
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/customizer.php';
*/


add_filter('wp_headers','fvn_add_origin');

function fvn_add_origin($headers) {
  $headers['Access-Control-Allow-Origin'] = "*";
  return $headers;  
}


add_filter('manage_posts_columns', 'posts_columns_id', 5);
add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);

add_filter('manage_pages_columns', 'posts_columns_id', 5);
add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);

function posts_columns_id($defaults){
    $defaults['wps_post_id'] = __('ID');
    return $defaults;
}

function posts_custom_id_columns($column_name, $id){
        if($column_name === 'wps_post_id'){
                echo $id;
    }
}



function spid_if_id(){
   global $post;
    $spid_check = $post->ID;

    if($spid_check == 2039){
        echo '<!--'.$spid_check.'-->';
        echo '<script id="spid-jssdk" src="https://www.fvn.no/resources/js/external/paywall/spid-sdk-1.7.6.min.js"></script>';
    } 
}

function spid_script() {
   global $post;
    $spid_check = $post->ID;

    if($spid_check == 2039){    
    echo "<script>jQuery(document).ready(function() { "
    . "VGS.Event.subscribe('auth.sessionChange', function(data){ if (data.hasOwnProperty('session')) {" 
  ."  jQuery('.your-id input').attr('value', data.session.userId); "
  ."  jQuery('.your-name input').attr('value', data.session.displayName); "
  ." }  }); "
    ." VGS.init({client_id: '4f911ef0411c7a1739000002', server: 'payment.schibsted.no', prod:true, logging:false, status:true}); "
    ." });</script>";
    }
    
}

// add_action('wp_footer', 'spid_script');
// add_action('wp_head', 'spid_if_id');

?>