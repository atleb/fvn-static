
<!DOCTYPE html>
<html lang="no">
<head>
  <meta charset="utf-8">
  <title>Fædrelandsvennen F+</title>
  <meta name="description" content="Fædrelandsvennen F+">
  <meta name="author" content="Rehan Anwar">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700,300,600,400' rel='stylesheet' type='text/css'>

  <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <?php 
        wp_register_script('modernizr', get_stylesheet_directory_uri() . '/js/modernizr.min.js', array('jquery'), false, false);
        wp_enqueue_script('modernizr');
    ?>
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="shortcut icon" href="http://www.fvn.no/skins/publications/fvn/gfx/icons/favicon.ico" />

<?php wp_head(); ?>
</head>
    
<body <?php body_class(); ?>>