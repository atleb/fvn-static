<?php
/*
Template Name: Standalone Page
v1.1 tweaks
*/
?>
<?php get_header(); ?>

    <div class="top-bar page-width">
        <a href="//www.fvn.no"><img width="280" src="<?php echo  get_stylesheet_directory_uri(); ?>/images/fvn-logo-white.svg" class="top-logo"></a>
    </div>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php the_content() ?>
    <?php endwhile; endif; ?>
    </div>


<?php wp_footer(); ?>

</body>
</html>