            

<?php 
    wp_register_script('main_js', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'), false, false);
    wp_enqueue_script('main_js');
?>

<?php wp_footer(); ?>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-10757236-1', 'fvn.no');
    ga('set', 'anonymizeIp', true);
    ga('set', 'dimension2',  'info'); 
    ga('set', 'contentGroup1',  'info');
    ga('send', 'pageview');
</script>

</body>
</html>