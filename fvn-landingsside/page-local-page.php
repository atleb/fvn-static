<?php
/*
Template Name: Local Page
v1.0 temp
*/
?>
<?php get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php the_content() ?>
    <?php endwhile; endif; ?>
    </div>

<?php wp_footer(); ?>

</body>
</html>