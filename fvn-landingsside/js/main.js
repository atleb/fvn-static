$(document).ready(function() {

  $(".box .read-more").click(function() {
    $(this).parent().children("ul").fadeToggle();

    // Toggle the read-more icon
    if ($(this).children(".icon-plus").html() == '+') {
      $(this).children(".icon-plus").html('&#150;');
    } else {
      $(this).children(".icon-plus").html('&#43;');
    }

  });

  $('.tooltip').tooltipster({
    theme: 'tooltip-theme-fvn',
    touchDevices: true,
    trigger: 'hover',
    contentAsHTML: true
  });

  if (!Modernizr.svg) {
    $('img[src$=".svg"]').each(function()
    {
      $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
    });
  }

});