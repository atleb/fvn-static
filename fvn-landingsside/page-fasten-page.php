<?php
/*
Template Name: Fasten Page
v0.1 poc
*/
?>
<link rel='stylesheet' href='<?php echo esc_url( get_stylesheet_uri() ) ?>' type='text/css' />
  
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php the_content() ?>
    </div>
<?php endwhile; endif; ?>