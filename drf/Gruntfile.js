/**
 * Grunt tasks local development of DrFront
 * Schibsted Tech Polska
 * minor fvn tweaks
 * */
module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json')
    });

    grunt.registerTask('reload', 'Reload html content from given url', function() {
        var fs = require('fs'),
            waterfall = require('async-waterfall'),
            http = require('http'),
            done = this.async(),
            options = {
                host: "fvndev1.medianorge.no",
                path: '/adtest/',
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
                }
            };

        http.request(options, function(res) {
            grunt.log.writeln("Got response: " + res.statusCode);
            var output = '';

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function() {
                waterfall([
                    function (callback) {
                        output = output.replace(/id="front-[1-9]+"/g, 'id="drfront"')
                                       .replace(/<link (.*)href="(?!http)/g, '$&http://fvndev1.medianorge.no')
                                       .replace('</head>',
                                                '<link rel="stylesheet" href="base.css">\n' +
                                                '<link rel="stylesheet" href="fvn_local_scss.css">\n$&');
                        callback();
                    },
                    function () {
                        if (!fs.existsSync('static')) {
                            fs.mkdirSync('static');
                        }

                        fs.writeFileSync('static/index.html', output);
                        done();
                    }
                ]);
            });
        }).on('error', function(e) {
            grunt.log.error("Got error: " + e.message);
            // Tell grunt the async task failed
            done(false);
        }).end();
    });

    grunt.registerTask('betaload', 'Reload html content from given url', function() {
        var fs = require('fs'),
            waterfall = require('async-waterfall'),
            http = require('http'),
            done = this.async(),
            options = {
                host: "www.fvn.no",
                path: '/beta/',
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
                }
            };

        http.request(options, function(res) {
            grunt.log.writeln("Got response: " + res.statusCode);
            var output = '';

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function() {
                waterfall([
                    function (callback) {
                        output = output.replace(/id="front-[1-9]+"/g, 'id="drfront"')
                                       .replace(/<link (.*)href="(?!http)/g, '$&http://www.fvn.no')
                                       .replace(/<title>\s/g, '<title>DEV:')
                                       .replace('</head>',
                                                '<link rel="stylesheet" href="base.css">\n' +
                                                '<link rel="stylesheet" href="../scratch.css">\n' +
                                                '<link rel="stylesheet" href="fvn_local_scss.css">\n$&');
                        callback();
                    },
                    function () {
                        if (!fs.existsSync('static')) {
                            fs.mkdirSync('static');
                        }

                        fs.writeFileSync('static/beta.html', output);
                        done();
                    }
                ]);
            });
        }).on('error', function(e) {
            grunt.log.error("Got error: " + e.message);
            // Tell grunt the async task failed
            done(false);
        }).end();
    });

};
