# SND api feeds config #
base url for fvn publication and section 1 = frontpage;
`http://api.snd.no/drfront/publication/fvn/fronts/1/auto´

see `http://api.snd.no/news/v1/publication/fvn/sections/´ for other ids(sport, kultur, ...)

* News only: `contentType=news&options=paywallstate´
* Feature: `contentType=feature&options=paywallstate,showheader´
* Reviews: `contentType=review&options=reviewscore,paywallstate´
* All options `options=reviewscore,paywallstate,showheader´


# Development env configuration #

1. install node.js (with npm)
2. install ruby
3. install bower by typing `npm install -g bower` (global installation)
4. install sass: `gem install sass`
5. clone repo `git clone https://bitbucket.org/bt-projects/drfront`
6. navigate to local directory with repo
7. in main dir type: `npm install`
8. to download most recent static html type: grunt reload
* if you want to change your request server (ex: http://dr.front.com/?yourParameter) you need to edit host and path properties within options object
* go to 17 line in Gruntifle.js file, and change it:
* host - your domain name, without protocole, ex: dr.front.com
* path - is everything that occurs after the slash, ex: /?yourParameter
9. create file watcher for sass compilation
* you have to be sure that render css file has name btpedanticsass.css and is located under /skins/BT directory
* be sure to add --sourcemap argument to enable creating css source maps for sass usage
* go to dev tools and enable it in your browser (for chrome in dev tools general settings you have flag "Enable CSS source maps" and "Auto-reload generated CSS" which have to be enabled
10. go with sass changes (on /skins/BT/btpedanticsass.scss file) and test it under /static/index.html file
11. for local dev change grunt task and file names/paths as needed
